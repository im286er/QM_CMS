<?php
/* 
* @Title:  [安装单入口]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-27 09:05:21
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-27 09:14:07
* @Copyright:  [hn7m.com]
*/
//定义安装目录常量
define('APP_NAME', 'Install');
//开启DEBUG
define('DEBUG', TRUE);
//引入核心文件
require '../../hdphp/hdphp/hdphp.php';
?>