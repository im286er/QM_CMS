<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html>
<html>
    <head>
        <title>CMS_EDU安装程序</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Install/./Tpl/Public/css/check.css"/>
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Install/./Tpl/Public/css/index.css"/>
</head>
<body>
    <div class="setup">
        <div class="title">
            CMS_EDU Install
        </div>
        <div class="copy">

            <p>
                <?php echo $setup['version'];?>
            </p>
            <h3><em>CMS_EDU | </em>安装向导</h3>
        </div>
        <div class="body">
            <h2>环境检测</h2>
            <table>
                <tr>
                    <th>名称</th>
                    <th>HDCMS要求配置</th>
                    <th>最佳配置</th>
                    <th>当前环境</th>
                </tr>
                <tr>
                    <td>操作系统</td>
                    <td>任意</td>
                    <td>类linux</td>
                    <td><?php echo $system['phpos'];?></td>
                </tr>
                <tr>
                    <td>PHP版本</td>
                    <td>5.0</td>
                    <td>5.3</td>
                    <td><?php echo $system['phpversion'];?></td>
                </tr>
                <tr>
                    <td>附件上传大小</td>
                    <td>不限制</td>
                    <td>5M</td>
                    <td><?php echo $system['uploadsize'];?></td>
                </tr>
                <tr>
                    <td>GD库</td>
                    <td>1.0</td>
                    <td>2.0</td>
                    <td><?php echo $system['gdversion'];?></td>
                </tr>
                <tr>
                    <td>磁盘空间</td>
                    <td>8MB</td>
                    <td>不限制</td>
                    <td><?php echo $system['diskspace'];?></td>
                </tr>
            </table>
            <!--目录检测-->
            <h2>环境检测</h2>
            <table style="width:90%">
                <tr>
                    <th>目录文件名称</th>
                    <th>所需状态</th>
                    <th>当前状态</th>
                </tr>
                <?php if(is_array($setup['dirs'])):?><?php  foreach($setup['dirs'] as $k=>$v){ ?>
                    <tr>
                        <td><?php echo $v;?></td>
                        <td>可写</td>
                        <td><?php echo is_writeable($v)?"<img src='http://localhost/HDPHP_CMS/Install/./Tpl/Public/images/10.png'/>&nbsp;可写":"<img src='http://localhost/HDPHP_CMS/Install/./Tpl/Public/images/12.png'/>不可写"?></td>
                    </tr>
                <?php }?><?php endif;?>
            </table>

            <!--函数检测-->
            <h2>环境检测</h2>
            <table style="width:80%">
                <tr>
                    <th width="280">目录名称</th>
                    <th>检测结果</th>
                    <th>系统建议</th>
                </tr>
                <?php if(is_array($setup['functions'])):?><?php  foreach($setup['functions'] as $k=>$v){ ?>
                    <tr>
                        <td><?php echo $v;?></td>
                        <td><?php echo function_exists($v)?"<img src='http://localhost/HDPHP_CMS/Install/./Tpl/Public/images/10.png'/>&nbsp;支持":"<img src='http://localhost/HDPHP_CMS/Install/./Tpl/Public/images/12.png'/>不支持"?></td>
                        <td>无</td>
                    </tr>
                <?php }?><?php endif;?>
            </table>
        </div>
        <div class="submit">
            <form action="" method="get">
                <input type="hidden" name="m" value="setconfig">
                <input type='submit' class='submit' value='下一步'/>
            </form>
        </div>
        <div class="step">
            <ul>
                <li style="color:#fff;font-weight:800">1.许可协议</li>
                <li style="color:#fff;">==></li>
                <li style="color:#fff;font-weight:800">2.环境检测</li>
                <li style="color:#fff;">==></li>
                <li>3.配置参数</li>
                <li>==></li>
                <li>4.安装数据库</li>
                <li>==></li>
                <li>5.完成安装</li>
            </ul>
        </div>
    </div>
</body>
</html>
