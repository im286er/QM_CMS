<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html>
<html>
    <head>
        <title>CMS_EDU安装程序</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Install/./Tpl/Public/css/finish.css"/>
     <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Install/./Tpl/Public/css/index.css"/>
    <base target="_blank"/>
</head>
<body>
    <div class="setup">
        <div class="title">
            CMS_EDU Install
        </div>
        <div class="copy">

            <p>
                <?php echo $setup['version'];?>
            </p>
            <h3><em>CMS_EDU | </em>安装向导</h3>
        </div>
        <div class="body">
            <h1>安装完成</h1>
            <h1>感谢您使用CMS_EDU系统</h1>
            <p><a href="<?php echo $url['front_url'];?>">访问首页</a></p>
            <p><a href="<?php echo $url['admin_url'];?>">进入后台管理</a></p>
            <br/>
            <span style="font-size:14px;display:block;clear:both;color:#89B928;padding-top:30px;">
                安全提示：安装完成后请将install目录删除
            </span>
        </div>

        <div class="step">
        </div>
    </div>
</body>
</html>
