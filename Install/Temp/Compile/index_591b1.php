<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
        <title>cms_edu安装程序</title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Install/./Tpl/Public/css/index.css"/>
</head>
<body>
    <div class="setup">
        <div class="title">
            CMS_EDU Install
        </div>
        <div class="copy">

            <p>
                <?php echo $setup['version'];?>
            </p>
            <h3><em>cms_edu | </em>安装向导</h3>
        </div>
        <div class="body">
            <div class="con">
                <?php echo $setup['license'];?>
            </div>
        </div>
        <div class="submit">
            <form action="" method="get">
                <input type="hidden" name="m" value="check"/>
                <input type='submit' value='同意协议' class='submit'/>
            </form>
        </div>
        <div class="step">
            <ul>
                <li style="color:#fff;font-weight:800">1.许可协议</li>
                <li style="color:#fff;">==></li>
                <li>2.环境检测</li>
                <li>==></li>
                <li>3.配置参数</li>
                <li>==></li>
                <li>4.安装数据库</li>
                <li>==></li>
                <li>5.完成安装</li>
            </ul>
            
        </div>
    </div>
</body>
</html>
