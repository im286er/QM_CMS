<?php
/* 
* @Title:  [后台登陆控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-18 11:01:33
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-18 22:18:12
* @Copyright:  [hn7m.com]
*/
class LoginControl extends Control{
	/**
	 * [index 默认载入后台登陆模板]
	 * @return [type] [description]
	 */
	public function index(){
		//echo md5("admin888");die();
		//print_const();die;
		if(IS_POST){
			//加载扩展模型（K 函数）,如果使用UserModel模型方法执行验证
			$UserModel=K('User');
			if(!$admin=$UserModel->validate_login()){
				$this->error($UserModel->error);
			}

			//写入session信息
			$_SESSION['adminid']=$admin['uid'];
			$_SESSION['adminname']=$admin['username'];

			$this->success('恭喜您,登陆成功!',U('Index/index'));
		}
		$this->display();
	}

	public function code(){
		$code=new Code();
		$code->show();
	}

	public function out(){
		session('adminid',NULL);
		session('adminname',NULL);
		
		$this->success('退出成功');
	}

}
?>