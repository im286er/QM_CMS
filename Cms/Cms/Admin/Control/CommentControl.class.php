<?php
/* 
* @Title:  [评论管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-28 15:32:54
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 16:38:16
* @Copyright:  [hn7m.com]
*/
class CommentControl extends CommonControl{
	private $model=NULL;

	public function __init(){
		parent::__init();
		$this->model=K('CommentView');
	}

	/**
	 * [index 默认显示的评论列表页]
	 * @return [type] [description]
	 */
	public function index(){
		$data=$this->model->get_data();
		$this->assign('comment',$data);
		$this->display();
	}

	public function del_comment(){
		if(IS_AJAX){
			$coid=Q('coid',0,'intval');
			if(K('comment')->del_data($coid)){
				$return=array(
					'state'		=>1,
					'message'	=>'恭喜您,删除成功',
					'timeout'	=>3
					);
				$this->ajax($return);
			}
		}
	}
}
?>