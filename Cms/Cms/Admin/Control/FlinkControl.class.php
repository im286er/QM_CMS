<?php
/* 
* @Title:  [友情链接控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-24 22:57:08
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 14:37:26
* @Copyright:  [hn7m.com]
*/
class FlinkControl extends CommonControl{
	private $model;

	/**
	 * [__init 自动加载Tag模型]
	 * @return [type] [description]
	 */
	public function __init(){
		parent::__init();
		$this->model=K('Flink');
	}

	/**
	 * [index 默认显示友情链接列表首页]
	 * @return [type] [description]
	 */
	public function index(){
		$flink=$this->model->get_data();
		$this->assign('flink',$flink);
		$this->display();
	}

	/**
	 * [add_flink 添加友情链接]
	 */
	public function add_flink(){
		if(IS_POST){
			if(!$this->model->create()){
				$this->error($this->model->error);
			}
			if (!$this->model->add_data()) {
				$this->error($this->model->error);
			}			
			$this->success('恭喜您,添加成功!');
		}
		$this->display();
	}

	public function del_flink(){
		if(IS_AJAX){
			$fid=Q('fid',0,'intval');
			if($this->model->del_data($fid)){
				$return=array(
					'state'		=>1,
					'message'	=>'恭喜您,删除成功',
					'timeout'	=>3
					);
				$this->ajax($return);
			}
		}
	}

	/**
	 * [ajax_update_stat 通过ajax修改是否显示的状态]
	 * @return [type] [description]
	 */
	public function ajax_update_stat(){
		if(IS_AJAX){
			$this->model->update_stat();
			$return = array(
					'state' => 1,
					'message' => '操作成功',
					'timeout'=>1
				);
			$this->ajax($return);
		}
	}

	public function edit_flink(){
		if (IS_POST) {
			//判断是否通过验证
			if(!$this->model->create()){
				$this->error($this->model->error);
			}
			//执行模型修改
			if(!$this->model->update_data()){
				$this->error($this->model->error);
			}
			$this->success('恭喜您,链接修改成功!',U('index'));
		}
		//分配数据到修改页面
		$flink=$this->model->get_one_data();
		$this->assign('flink',$flink);
		$this->display();
	}
}
?>