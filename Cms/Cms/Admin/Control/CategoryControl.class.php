<?php
/* 
* @Title:  [后台分类管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-19 15:44:28
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 17:40:53
* @Copyright:  [hn7m.com]
*/

class CategoryControl extends CommonControl{
	//保存K以后的模型
	private $model;

	//自动加载目录分类模型
	public function __init(){
		parent::__init();
		$this->model=K('Category');
	}

	/**
	 * [index 显示分类类别]
	 * @return [type] [description]
	 */
	public function index(){
		$data = $this->model->get_all();
		//$data = $this->limitless($data);
		//取得分页数据，并且分配
		// $page = $data['page'];
		// $this->assign('page', $page);

		// //卸载掉分页数据，避免循环出错
		// unset($data['page']);

		$data= Data::tree($data,'cname','cid','pid');
		$data = Data::channelList($data, 0, '&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp', 'cid','pid');
		$this->assign('data', $data);

		$this->display();
	}

	// public function limitless($data,$pid=0,$level=0){
	// 	$arr = array();
	// 	foreach ($data as $v) {

	// 		if($v['pid'] == $pid){
	// 			$v['level'] = $level;
	// 			$v['html'] = str_repeat('_', ($level * 9));
	// 			$arr[] = $v;
	// 			$arr = array_merge($arr, $this->limitless($data, $v['cid'],$level + 1));
	// 		}
	// 	}

	// 	return $arr;
	// }

	/**
	 * [add 添加分类]
	 */
	public function add(){

		if(IS_AJAX){

			//执行验证
			if(!$this->model->create()){
				$return = array(
					'state' => 0,
					'message' => $this->model->error,
					'timeout'=>3
				);
				//就是返回给异步数据
				//然后ajax根据state做出判断，成功与否
				//用hd框架，就用$this->ajax()这样的方法返回给异步数据
				$this->ajax($return);
			}

			//执行模型添加方法
			if($this->model->add_data()){
				$return = array(
					'state' => 1,
					'message' => '添加成功',
					'timeout'=>3
				);
				$this->ajax($return);
			}
			
		}
		//增加子级分类
		$field=array('cid','cname');
		$cid=Q('cid',0,'intval');
		$data=$this->model->get_one_data($cid,$field);

		$this->assign('data',$data);
		$this->display();
	}

	/**
	 * [ajax_update_stat 通过ajax修改
	 * is_listhtml,is_arcthml,is_show他们的状态]
	 * @return [type] [description]
	 */
	public function ajax_update_stat(){
		if(IS_AJAX){
			$this->model->update_stat();
			$return = array(
					'state' => 1,
					'message' => '操作成功',
					'timeout'=>3
				);
			$this->ajax($return);
		}
	}

	/**
	 * [del 执行删除动作]
	 * @return [type] [description]
	 */
	public function del(){
		if(IS_AJAX){
			$cid = Q('cid', 0, 'intval');
			if(!$this->model->have_data($cid)){
				$return = array(
					'state' => 0,
					'message' => $this->model->error,
					'timeout'=>3
				);
				//就是返回给异步数据
				//然后ajax根据state做出判断，成功与否
				//用hd框架，就用$this->ajax()这样的方法返回给异步数据
				$this->ajax($return);
			}

			//执行删除
			$this->model->del_data($cid);
			$return = array(
					'state' => 1,
					'message' => '删除成功',
					'timeout'=>3
				);
			//就是返回给异步数据
			//然后ajax根据state做出判断，成功与否
			//用hd框架，就用$this->ajax()这样的方法返回给异步数据
			$this->ajax($return);
		}
	}

	/**
	 * [edit 编辑分类]
	 * @return [type] [description]
	 */
	public function edit(){
		if(IS_AJAX){
			if(!$this->model->create()){
				$return = array(
					'state' => 0,
					'message' => $this->model->error,
					'timeout'=>3
				);
				//就是返回给异步数据
				//然后ajax根据state做出判断，成功与否
				//用hd框架，就用$this->ajax()这样的方法返回给异步数据
				$this->ajax($return);
			}
			//执行模型修改方法
			$this->model->update_data(Q('post.cid', 0, 'intval'));
			$return = array(
					'state' => 1,
					'message' => '修改成功',
					'timeout'=>3
				);
			$this->ajax($return);
		}
		//获得单条数据
		$cid = Q('cid', 0, 'intval');
		$data = $this->model->get_one_data($cid);
		$this->assign('data', $data);

		//分配用户所选分类
		$cate = $this->model->get_chose_cate($cid);
		$this->assign('cate', $cate);

		$this->display();
	}


}
?>