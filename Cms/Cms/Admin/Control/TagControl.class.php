<?php
/* 
* @Title:  [标签控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-20 16:50:58
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-20 20:14:11
* @Copyright:  [hn7m.com]
*/
class TagControl extends CommonControl{
	private $model;

	/**
	 * [__init 自动加载Tag模型]
	 * @return [type] [description]
	 */
	public function __init(){
		parent::__init();
		$this->model=K('Tag');
	}
	/**
	 * [index 默认显示标签列表]
	 * @return [type] [description]
	 */
	public function index(){
		//通过ajax修改
		if(IS_AJAX){
		
			//执行模型修改动作
			$this->model->update_data();
			$return = array(
				'state' => 1,
				'message' => '修改成功',
				'timeout'=>3
			);
			//就是返回给异步数据
			//然后ajax根据state做出判断，成功与否
			//用hd框架，就用$this->ajax()这样的方法返回给异步数据
			$this->ajax($return);
		}
		$data=$this->model->get_data();
		$this->assign('data',$data);
		$this->display();
	}

	/**
	 * [add_tag 添加标签]
	 */
	public function add_tag(){
		if(IS_AJAX){
			//执行验证
			if(!$this->model->create()){
				$return = array(
					'state'=>0,
					'message'=>$this->model->error,
					'timeout'=>3
				);
				$this->ajax($return);
			}
			if($this->model->add_data()){
				$return = array(
					'state'=>1,
					'message'=>'添加成功!',
					'timeout'=>3
				);
			}else{
				$return =array(
					'state'=>0,
					'message'=>$this->model->error,
					'timeout'=>3
				);
			}
			$this->ajax($return);
		}
		$this->display();
	}

	/**
	 * [del_tag 删除标签]
	 * @return [type] [description]
	 */
	public function del_tag(){
		$this->model->del_data(Q('tid',0,'intval'));
		$this->success('删除成功!');
	}
}
?>