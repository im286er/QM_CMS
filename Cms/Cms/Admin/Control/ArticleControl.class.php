<?php
/* 
* @Title:  [文章管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-21 15:13:04
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 18:28:26
* @Copyright:  [hn7m.com]
*/

class ArticleControl extends Control{
	private $model;
	/**
	 * [__init 自动连接数据库]
	 * @return [type] [description]
	 */
	public function __init(){
		parent::__init();
		$this->model=K('Article');
	}
	/**
	 * [index 默认显示模板]
	 * @return [type] [description]
	 */
	public function index(){

		$article=K('ArticleView')->join('category')->get_data();
		$this->assign('article',$article);
		//p($article);die;

		$this->display();
	}
	/**
	 * [add 添加文章]
	 */
	public function add(){
		if(IS_POST){
			//执行自动验证与自动完成方法
			if(!$this->model->create()){
				$this->error($this->model->error);
			}
			//执行模型添加
			if(!$this->model->add_data()){
				$this->error($this->model->error);
			}
			//$this->success('添加成功',U('go_page'),0);
			go(U('go_page'));
		}

		//分配所有分类
		$category=K('Category')->get_data();
		$category=Data::channelList($category,0,'-','cid','pid');
		//p($category);die();
		$this->assign('category',$category);

		//分配标签
		$tag=K('tag')->get_data();
		$this->assign('tag',$tag);

		$this->display();
	}
	/**
	 * [go_page 文章发布成功后选择要跳转的页面]
	 * @return [type] [description]
	 */
	public function go_page(){
		$this->display();
	}

	/**
	 * [edit 修改文章]
	 * @return [type] [description]
	 */
	public function edit(){
		//**********************分配开始*****************************************//
		$aid = Q('aid', 0, 'intval');
		$article=K('ArticleView')->get_one_data($aid);
		//分配文章属性,变成数组.重新压回
		$article['attr']=(isset($article['attr']))?explode(',', $article['attr']):NULL;
		//p($article);die();
		$this->assign('article',$article);

		//分配所有分类
		$category=K('Category')->get_data();
		$category=Data::channelList($category,0,'-','cid','pid');
		//删掉原有的分类数据
		$cate=array();
		$cate_self=$article['cname'];
		foreach ($category as $key => $value) {
			if($value['cname']==$cate_self){
		        unset($value);
			}else{
				$cate[]=$value;
			}
		}
		$this->assign('category',$cate);

		//分配标签
		$tag=K('tag')->get_data();
		$article_tag=K('ArticleTag')->where(array('article_aid'=>$aid))->findAll();
		$tag_tids=array();
		if(is_array($article_tag)){
			foreach ($article_tag as $key => $value) {
				$tag_tids[]=$value['tag_tid'];
			}
		}
		//p($tag_tids); die;
		$this->assign('tag_tids',$tag_tids);
		$this->assign('tag',$tag);
		//**********************分配结束*****************************************//
		
		//**********************修改开始*****************************************//
		if(IS_POST){
			//判断是否通过验证
			if(!$this->model->create()){
				$this->error($this->model->error);
			}
			//执行模型修改
			if(!$this->model->update_data()){
				$this->error($this->model->error);
			}
			$this->success('恭喜您,文章修改成功!',U('index'));
		}
		//**********************修改结束*****************************************//
		$this->display();
	}

	/**
	 * [to_recycle 删除文章到回收站]
	 * @return [type] [description]
	 */
	public function to_recycle(){
		if(IS_AJAX){
			$aid = Q('aid', 0, 'intval');
			//执行删除
			if($this->model->recycle($aid)){
			$return = array(
					'state' => 1,
					'message' => '恭喜您,删除到回收站成功!',
					'timeout'=>3
				);
			//就是返回给异步数据
			//然后ajax根据state做出判断，成功与否
			//用hd框架，就用$this->ajax()这样的方法返回给异步数据
			$this->ajax($return);
			}
		}
	}

	/**
	 * [recycle 回收站模板]
	 * @return [type] [description]
	 */
	public function recycle(){
		$bin=K('ArticleView')->join('category')->get_data($recycle=1);
		$this->assign('bin',$bin);
		$this->display();
	}
	/**
	 * [recovery 从回收站还原]
	 * @return [type] [description]
	 */
	public function recovery(){
		if(IS_AJAX){
			$aid = Q('aid', 0, 'intval');
			//执行还原
			if($this->model->recoveried($aid)){
			$return = array(
					'state' => 1,
					'message' => '恭喜您,还原成功!',
					'timeout'=>3
				);
			//就是返回给异步数据
			//然后ajax根据state做出判断，成功与否
			//用hd框架，就用$this->ajax()这样的方法返回给异步数据
			$this->ajax($return);
			}
		}
	}

	/**
	 * [del 彻底删除文章,同时删除与之关联的一切信息和图片]
	 * @return [type] [description]
	 */
	public function del(){
		if(IS_AJAX){
			$aid = Q('aid', 0, 'intval');
			//执行彻底删除
			if($this->model->del_data($aid)){
			$return = array(
					'state' => 1,
					'message' => '恭喜您,删除成功!',
					'timeout'=>3
				);
			//就是返回给异步数据
			//然后ajax根据state做出判断，成功与否
			//用hd框架，就用$this->ajax()这样的方法返回给异步数据
			$this->ajax($return);
			}
		}
	}

}
?>