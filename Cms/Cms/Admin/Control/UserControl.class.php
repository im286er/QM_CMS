<?php
/* 
* @Title:  [用户控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-28 16:45:23
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 20:40:49
* @Copyright:  [hn7m.com]
*/
class UserControl extends Control{
	private $model;

	public function __init(){
		parent::__init();
		$this->model=K('User');
	}

	/**
	 * [foreground 前台用户]
	 * @return [type] [description]
	 */
	public function foreground_user(){
		$where=array('is_admin'=>0);
		$data=$this->model->get_data($where);
		$this->assign('foreground',$data);
		$this->display();
	}

	/**
	 * [background_admin 后台用户]
	 * @return [type] [description]
	 */
	public function background_admin(){
		$where=array('is_admin'=>1);
		$data=$this->model->get_data($where);
		$this->assign('background',$data);
		$this->display();
	}

	/**
	 * [add_user 添加用户]
	 */
	public function add_user(){
		if(IS_POST){
			if(!$this->model->create()) $this->error($this->model->error);
			$url= (Q('post.is_admin')==1)? U('background_admin'):U('foreground_user');
			if($this->model->add_user()) $this->success('恭喜您,用户添加成功!',$url);
		}
		$this->display();
	}

	/**
	 * [del_user 删除用户]
	 * @return [type] [description]
	 */
	public function del_user(){
		if(IS_AJAX){
			$uid=Q('uid',0,'intval');
			if($this->model->del_data($uid)){
				$return=array(
					'state'		=>1,
					'message'	=>'恭喜您,删除成功',
					'timeout'	=>2
					);
				$this->ajax($return);
			}
		}
	}

	/**
	 * [lock_user 锁定用户]
	 * @return [type] [description]
	 */
	public function lock_user(){
		if($this->model->mdf_lock(TRUE)) $this->success('操作成功!');
	}

	/**
	 * [unlock_user 解锁用户]
	 * @return [type] [description]
	 */
	public function unlock_user(){
		if($this->model->mdf_lock(FALSE)) $this->success('操作成功!');
	}


}
?>