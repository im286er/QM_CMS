<?php
/* 
* @Title:  [网站配置控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-27 12:24:36
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-27 14:08:22
* @Copyright:  [hn7m.com]
*/
class WebsetControl extends CommonControl{
	/**
	 * [index 显示所有的配置与修改配置]
	 * @return [type] [description]
	 */
	public function index(){
		$WebSetModel=K('Webset');

		//分配数据
		$data=$WebSetModel->get_data();
		$temp=array();
		//重新组合为三维数组
		foreach ($data as $key => $value) {
			$temp[$value['type_id']][]=$value;
		}
		//p($temp);die();
		$this->assign('data',$temp);

		//遍历Templates下面所有的模板文件夹,以便可以随意更换选择模板
		$dir=Dir::tree(ROOT_PATH.'Templates');
		$this->assign('dir',$dir);

		//执行模型修改方法
		if(IS_AJAX){
			$WebSetModel->save_data();
			$return=array(
				'state'=>1,
				'message'=>'恭喜您,修改成功!',
				'timeout'=>2
				);
			//获得修改过的数据
			$data=$WebSetModel->get_data();
			//经过循环变成一维数组
			$temp=array();
			foreach ($data as $key => $value) {
				$temp[$value['name']]=$value['value'];
			}
			$temp=var_export($temp,TRUE);
			//写入配置文件
			$str=<<<str
<?php
if (!defined("HDPHP_PATH"))exit('No direct script access allowed');
return {$temp}
?>
str;
			file_put_contents(COMMON_CONFIG_PATH."webconfig.php", $str)||$return=array(
				'state'=>0,
				'message'=>'没有权限!',
				'timeout'=>2
				);			
			$this->ajax($return);
		}
		$this->display();
	}
}
?>