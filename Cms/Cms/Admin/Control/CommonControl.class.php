<?php
/* 
* @Title:  [通用控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-18 11:02:09
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 12:09:42
* @Copyright:  [hn7m.com]
*/

class CommonControl extends Control{

	/**
	 * [__init 自动检测是否登陆,要求必须登陆后才能进入后台]
	 * @return [type] [description]
	 */
	public function __init(){
		if(!isset($_SESSION['adminid']) || !isset($_SESSION['adminname'])){
			go(U('Login/index'));
		}
	}

	/**
	 * [go_success 用于生成首页静态]
	 * @return [type] [description]
	 */
	public function go_success(){
		$msg=Q('get.msg');
		$this->success($msg,U('Admin/Index/copy'));
	}

	/**
	 * [go_error 用于静态页生成失败]
	 * @return [type] [description]
	 */
	public function go_error(){
		$msg=Q('get.msg');
		$this->error($msg);
	}

	/**
	 * [one_key 用于一键生成全站静态]
	 * @return [type] [description]
	 */
	public function one_key(){
		$msg=Q('get.msg');
		$this->success($msg,U('Index/Html/list_'));
	}
	/**
	 * [go_middle 用于从列表页到内容页的过渡]
	 * @return [type] [description]
	 */
	public function go_middle(){
		$msg=Q('get.msg');
		$this->success($msg,U('Index/Html/details'));
	}
}
?>