<?php
/* 
* @Title:  [数据库备份管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-27 21:59:34
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 21:46:30
* @Copyright:  [hn7m.com]
*/
class BackUpControl extends CommonControl{
	/**
	 * [index 备份列表与还原动作]
	 * @return [type] [description]
	 */
	public function index(){
		$dir=Dir::tree(ROOT_PATH.'Backup');
		$this->assign('dir',$dir);
		// p($dir);die();
		$this->display();
	}

	public function recovery(){
		Backup::recovery(array('dir'=>'backup/'.Q('dir')));
		$this->dir=Q('dir');
	}

	/**
	 * [backup 执行备份动作]
	 * @return [type] [description]
	 */
	public function backup(){
		Backup::backup(array('dir'=>C('DB_BACKUP')));
	}

	/**
	 * [del_data 执行删除备份的动作]
	 * @return [type] [description]
	 */
	public function del_data(){
		if(IS_AJAX){
			$dir=Q('dir');
			if(Dir::del($dir)){
				$return=array(
					'state'		=>1,
					'message'	=>'恭喜您,删除成功',
					'timeout'	=>2
					);
				$this->ajax($return);
			}
		}
	}
}
?>