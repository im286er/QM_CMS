/* 
* @Author: nic-tes
* @Date:   2014-03-12 16:58:42
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-20 20:20:12
*/

$(document).ready(function(){
    var flag = false;
    $('.edit_tag').click(function(){
        var trObj = $(this).parents('tr');

        if(!flag){
            trObj.find('input[type=text]').show();
            trObj.find('span').hide();

            $(this).removeClass('btn-primary').addClass('btn-success').html('保存');
            flag = true;
        }else{
           trObj.find('form').submit();

        }
       

   })
});