/* 
* @Author: nic-tes
* @Date:   2014-03-11 23:44:01
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-20 16:37:59
*/

$(function(){
	//剩余所有顶级分类
	$('tr[pid!=0]').hide();
	//点击加号出现相对的子级分类，再次点击缩回
	$('.plus_minus').toggle(function() {
		var cid=$(this).parents('tr').attr('cid');
		$('tr[pid='+cid+']').show();
		$(this).removeClass('btn-success').addClass('btn-info').html('-');
	}, function() {
		var cid=$(this).parents('tr').attr('cid');
		$('tr[pid='+cid+']').hide();
		$(this).removeClass('btn-info').addClass('btn-success').html('+');
	});
})