<?php
/* 
* @Title:  [首页注册控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-26 21:04:30
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-26 22:15:05
* @Copyright:  [hn7m.com]
*/
class RegisterControl extends CommonControl{

	/**
	 * [reg 注册用户,并添加进数据库user表]
	 * @return [type] [description]
	 */
	public function reg(){
		if(IS_POST){
			$UseModel=K('User');
			//执行验证
			if(!$UseModel->validate_reg()) $this->error($UseModel->error);
		}

		$username=Q('post.username_reg');
		//组合数据
		$data=array(
			'username'=>$username,
			'password'=>Q('post.password_reg1','','md5'),
			'nickname'=>Q('post.nickname')
			);
		//注册用户
		$uid=K('User')->add_user($data);
		//写入session
		session('uid',$uid);
		session('username',$username);

		$this->success('恭喜您,注册成功!',__HISTORY__);
	}
	/**
	 * [code 生成验证码]
	 * @return [type] [description]
	 */
	public function code(){
		$code=new Code();
		$code->show();
	}

	/**
	 * [check_user 异步检测用户名是否存在]
	 * @return [type] [description]
	 */
	public function check_user(){
		if(!IS_AJAX) halt('非法请求!');
		if(K('User')->check_user(Q('post.username_reg'))) $this->ajax(0);
		$this->ajax(1);
	}

	public function check_code(){
		if(!IS_AJAX) halt('非法请求!');
		if(Q('post.code','','strtoupper') !=session('code')) $this->ajax(0);
		$this->ajax(1);
	}
}
?>