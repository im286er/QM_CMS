<?php
/* 
* @Title:  [登陆控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-26 22:24:01
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 20:20:47
* @Copyright:  [hn7m.com]
*/

class LoginControl extends Control{
	/**
	 * [login 登陆]
	 * @return [type] [description]
	 */
	public function login(){
		if(IS_POST){
			$UserModel=K('User');
			if(!$user=$UserModel->validate_login(false)) $this->error($UserModel->error);
			//写入session
			session('uid',$user['uid']);
			session('username',$user['username']);
			$this->success('恭喜您,登陆成功',__HISTORY__);
		}
	}
	/**
	 * [check_login 异步检测是否已经登陆]
	 * @return [type] [description]
	 */
	public function check_login(){
		if(!IS_AJAX) halt('非法请求');
		if(!isset($_SESSION['uid'])||!isset($_SESSION['username'])){
			$str=<<<str
			<a href="javascript:showLoginBox()" class="top_login">用户登录</a>
			<a href="javascript:showRegBox();">注册</a>
str;
		$this->ajax($str,'html');
		}
		//如果登陆以后
		$username=session('username');
		$out=U('out');
		$str=<<<str
		<span>{$username}</span><a href="{$out}">退出</a>
str;
		$this->ajax($str,'html');
	}

	/**
	 * [out 退出登陆]
	 * @return [type] [description]
	 */
	public function out(){
		session('uid',null);
		session('username',null);
		$this->success('退出成功',__HISTORY__);
	}
}
?>