<?php
/* 
* @Title:  [文章列表页控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-23 17:03:08
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 10:38:30
* @Copyright:  [hn7m.com]
*/
class ListControl extends CommonControl{
	public function index(){
		$data = K('Category')->get_one_data(Q('get.cid', 0, 'intval'),'cname');
		$this->assign('cname', $data['cname']);
		$this->dis('list');
	}
}
?>