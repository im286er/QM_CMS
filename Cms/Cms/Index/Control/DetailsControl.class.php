<?php
/* 
* @Title:  [文章管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-24 17:30:37
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-29 21:17:14
* @Copyright:  [hn7m.com]
*/
class DetailsControl extends CommonControl
{
	public function index(){
		$aid=Q('get.aid',0,'intval');
		//调取文章数据
		$data=K('ArticleView')->get_one_data($aid);
		//p($data);die();
		//所在栏目地址
		$data['url']=U('List/index',array('cid'=>$data['cid']));
		$this->assign('article',$data);
		//p($comment);die();
		$this->dis('details');
	}

	/**
	 * [get_click 动态获得点击次数,并且点击次数加1,用在内容页]
	 * @return [type] [description]
	 */
	public function get_click(){
		$aid=Q('get.aid',0,'intval');
		$click=K('Article')->get_click($aid,1);
		echo "document.write($click)";
		die;
	}

	/**
	 * [get_statics_click 动态获得点击次数,但点击次数不变,用在首页显示]
	 * @return [type] [description]
	 */
	public function get_statics_click(){
		$aid=Q('get.aid',0,'intval');
		$click=K('Article')->get_click($aid,0);
		echo "document.write($click)";
		die;
	}

	/**
	 * [get_commend 异步显示所有评论]
	 * @return [type] [description]
	 */
	public function get_commend(){
		if (IS_AJAX) {
			$aid = Q('get.aid', 0, 'intval');
			$comment = K('CommentView')->get_arc_comment($aid);
			$this->ajax($comment,'json');
		}
	}

	/**
	 * [comment 异步发表评论]
	 * @return [type] [description]
	 */
	public function comment(){
		if(IS_AJAX){
			$comment=K('Comment');
			$aid=Q('get.aid',0,'intval');
			$uid=Q('session.uid',NULL,'intval');
			$username=K('User')->get_username($uid);
			$username=($username)?$username:'匿名用户';
			$content=Q('post.comcon');
			if(!$comment->create()){
				echo NULL;return;
			}elseif ($comment->add_data()) {
				$add_com = <<<str
<ul class="position comtext">
	<li>
		用户名：{$username}
	</li>
	<li>
		评论内容:{$content}
	</li>
</ul>
str;
    		echo $add_com;return;
			}
		}
	}

	/**
	 * [commend_num 动态获得评论数]
	 * @return [type] [description]
	 */
	public function commend_num(){
		$aid=Q('get.aid',0,'intval');
		$comment=K('Comment')->total_comment($aid);
		echo "document.write($comment)";
		die;
	}

}
?>