<?php
/* 
* @Title:  [前台公共控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-23 15:34:39
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 10:57:23
* @Copyright:  [hn7m.com]
*/
class CommonControl extends Control{
	
	public function __init(){	
		if (!defined('__TEMPLATE_TPL__')) {
			define('__TEMPLATE_TPL__', __ROOT__.'/Templates/'.C('TPL_STYLE'));
		}
		if (!defined('TEMPLATE_TPL_PATH')) {
			define('TEMPLATE_TPL_PATH', ROOT_PATH.'Templates/'.C('TPL_STYLE'));
		}
	}

	/**
	 * [dis 定义载入模板的方法,因为框架中的载入模板的方法为display,所以自定义的dis方法不能与系统的重名]
	 * @param  [type] $tpl [description]
	 * @return [type]      [description]
	 */
	public function dis($tpl=NULL){
		if(is_null($tpl)){
			$tpl=TEMPLATE_TPL_PATH."/". METHOD . '.html';
		}else{
			//如果传参,例如传入了ndex.html.则使用参数的模板
            $suffix = strrchr($tpl,'.');
            //有可能用户传入的文件名只是ndex没有后缀,那么给他加个后缀
            $tpl = empty($suffix) ? $tpl . '.html' : $tpl;
			$tpl=TEMPLATE_TPL_PATH."/".$tpl;
		}
		$this->display($tpl);
	}

	public function suc(){
		
	}
}
?>