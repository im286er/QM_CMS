<?php
/* 
* @Title:  [文章管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-24 17:30:37
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 12:07:26
* @Copyright:  [hn7m.com]
*/
class HtmlControl extends CommonControl{
	
	/**
	 * [to_index 生成首页静态]
	 * @return [type] [description]
	 */
	public function to_index(){
		if($this->_create_html('Index/index','./index.html')){
			// $this->success('首页静态页面生成成功!',U('Admin/Index/copy'),1);
			go(U("Admin/Common/go_success/msg=首页静态页面生成成功"));
		}
		go(U("Admin/Common/go_error/msg=首页静态页面生成失败"));
	}

	/**
	 * [to_list 生成列表页静态]
	 * @return [type] [description]
	 */
	public function to_list(){
		$field=array('cid','htmldir','is_listhtml');
		$cateData=K('Category')->get_data($field);
		// p($cateData);die();
		foreach ($cateData as $key => $value) {
			//判断是否静态
			if(!$value['is_listhtml']) continue;
			//赋值给get
			$_GET['cid']=$value['cid'];
			if(isset($_GET['page'])){
				unset($_GET['page']);
			}
			//生成静态
			if ($this->_create_html('List/index',STATIC_PATH.$value['htmldir'].'/index.html')) {
				// p(STATIC_PATH.$value['htmldir'].'/index.html');
				// 生成分页静态
				for ($i=1; $i <= Page::$staticTotalPage; $i++) { 
					$_GET['page']=$i;
					$this->_create_html('List/index',STATIC_PATH.$value['htmldir'].'/'.$i.'.html');
				}
			}else{
				go(U("Admin/Common/go_error/msg=列表页静态页面生成失败"));
			}		
		}
		go(U("Admin/Common/go_success/msg=列表页静态页面生成成功"));
	}

	/**
	 * [details 生成内容页静态]
	 * @return [type] [description]
	 */
	public function details(){
		$field=array('aid','is_archtml','htmldir');
		$arcData=K('ArticleView')->get_join_data('category',$field);
		//p($arcData);die();
		foreach ($arcData as $key => $value) {
			//判断是否静态
			if(!$value['is_archtml']) continue;
			//赋值给get
			$_GET['aid']=$value['aid'];
			//生成静态
			$this->_create_html('Details/index',STATIC_PATH.$value['htmldir'].'/arc/'.$value['aid'].'.html')||go(U("Admin/Common/go_error/msg=内容页静态页面生成失败"));
		}
		go(U("Admin/Common/go_success/msg=内容页静态页面生成成功"));
	}

	/**
	 * [index 一键生成全站静态]
	 * @return [type] [description]
	 */
	public function index(){
		if($this->_create_html('Index/index','./index.html')){
			// go(U("Admin/Common/go_success/msg=首页静态页面生成成功"));
			// go(U('Index/Html/to_list'));
			go(U("Admin/Common/one_key/msg=首页静态页面生成成功"));
		}
		go(U("Admin/Common/go_error/msg=首页静态页面生成失败"));
	}

	
	/**
	 * [list_ 从生成列表页到内容页中间桥梁静态]
	 * @return [type] [description]
	 */
	public function list_(){

		$field=array('cid','htmldir','is_listhtml');
		$cateData=K('Category')->get_data($field);
		// p($cateData);die();
		foreach ($cateData as $key => $value) {
			//判断是否静态
			if(!$value['is_listhtml']) continue;
			//赋值给get
			$_GET['cid']=$value['cid'];
			if(isset($_GET['page'])){
				unset($_GET['page']);
			}
			//生成静态
			if ($this->_create_html('List/index',STATIC_PATH.$value['htmldir'].'/index.html')) {
				// p(STATIC_PATH.$value['htmldir'].'/index.html');
				//生成分页静态
				for ($i=1; $i <= Page::$staticTotalPage; $i++) { 
					$_GET['page']=$i;
					$this->_create_html('List/index',STATIC_PATH.$value['htmldir'].'/'.$i.'.html');
				}
			}else{
				go(U("Admin/Common/go_error/msg=列表静态页面生成失败"));
			}	
		}
		go(U("Admin/Common/go_middle/msg=列表页静态页面生成成功"));
	}

	// $CM, $file
	private function _create_html($CM, $file){

		//开启缓冲区******
		ob_start();
		//在缓冲区执行任何动作，都不会输出
		// 'Index/Index/index'
		A('Index/' . $CM);
		//获得缓冲区内容
		$data = ob_get_contents();
		//清除缓冲区********
		ob_clean();
		$path = dirname($file);
		is_dir($path) || mkdir($path, 0777, TRUE);
		//写入静态文件
		return file_put_contents($file,$data);
	}
}
?>
