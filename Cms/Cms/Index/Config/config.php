<?php
if (!defined("HDPHP_PATH"))exit('No direct script access allowed');
//更多配置请查看hdphp/Config/config.php
return array(
	'TPL_STYLE'                     => C('index_tpl_style'),          //风格
	'TPL_TAGS'                      => array('HtmlTag'),     //扩展标签,多个标签用逗号分隔
	'AUTO_LOAD_FILE'                => array('functions.php'),
);
?>