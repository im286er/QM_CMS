<?php
/* 
* @Title:  [前台自定义函数集合]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-25 20:50:30
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 22:56:50
* @Copyright:  [hn7m.com]
*/
function get_arc_url($ishtml,$htmldir,$aid){
	if ($ishtml) {
		$str=__ROOT__."/".STATIC_PATH."{$htmldir}/arc/".$aid.".html";
	}else{
		$str=U('Details/index',array('aid'=>$aid));
	}
	return $str;
}

function get_cate_url($ishtml,$htmldir,$cid){
	if ($ishtml) {
		$str=__ROOT__."/".STATIC_PATH.$htmldir."/"."index.html";
	}else{
		$str=U('List/index',array('cid'=>$cid));
	}
	return $str;
}

?>