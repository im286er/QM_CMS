<?php
/* 
* @Title:  [创建自定义标签类]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-23 16:41:45
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-29 16:41:33
* @Copyright:  [hn7m.com]
*/
/* 
* @Title:  	[<arclist></arclist>块标签参数汇总]
* @rows:   	设定需要获取的条数.例如:rows="10"
* @attr:   	设定文章属性.例如:attr="置顶"
* @order:  	设定排序规则.例如:order="aid DESC",默认发布时间倒序排列
* @cid:    	设定文章栏目.例如:cid="2"
* @pagerows:设定每页显示多少条.例如:pagerows="1"
* @pagenum: 设定页码数.例如:pagenum="3",默认5
* @tid:  	设定所属标签.例如:tid="2"
* @Title:  	[<arclist></arclist>块标签变量汇总]
* @{$field.total}:  	获得总的评论数
* @{$field.title}		获得文章标题
* @{$field.url}			获得文章地址
* 等等等等等等...
*/
/* 
* @Title:  	[<channel></channel>块标签参数汇总]
* @rows:   	设定需要获取的条数.例如:rows="10"
* @order:  	设定排序规则.例如:order="csort DESC",默认按cid顺序排列
* @csort:  	设定分类排序字段
* @cid:    	设定文章栏目.例如:cid="2"
* @type:  	son 调用子栏目;self 调用同级栏目;top 顶级栏目.默认self
* @Title:  	[<channel></channel>块标签变量汇总]
* @{$field.cname}		获得栏目名称
* @{$field.url}			获得栏目地址,跳转到该栏目的列表页
* @Copyright:  [hn7m.com]
*/
class HtmlTag{
	//定义块标签名
	public $tag=array(
		'arclist'=>array('block'=>1),
		'taglist'=>array('block'=>1),
		'channel'=>array('block'=>1),
		'flink'=>array('block'=>1),
		'feedback'=>array('block'=>1)
		);

	/**
	 * [_arclist 定义文章列表标签]
	 * @param  [type] $attr    [标签属性]
	 * @param  [type] $content [标签内容]
	 * @param  [type] $parse   [没有解析过的标签体]
	 * @return [type]          [description]
	 */
	public function _arclist($attr,$content,$parse){
		//**************************定义标签属性$attr开始****************************//
		//定义所取条数
		$rows= isset($attr['rows'])?$attr['rows']:NULL;
		//定义文章属性,比如说:热门
		$attrs= isset($attr['attr'])?$attr['attr']:NULL;
		//定义排序
		$order=isset($attr['order'])?".".$attr['order'].".":'"sendtime DESC"';

		//必须该分类显示并且文章必须没有进入回收站
		$where='is_show=1 and is_recycle=0';

		if($attrs){
			//组合属性where条件
			$where=' and attr like "%'.$attrs.'%"';
		}

		//定义分类id
		if(isset($attr['cid'])){
			//此处的三元表达式有两个作用:1允许用户设定cid="all",这样在列表页也能循环出全局的文章;2防止用户设定的cid中没有该栏目的而报错
			$cid=($attr['cid'] != 'all')?(int)$attr['cid']:NULL;
		}else if($getCid=Q('get.cid',0,'intval')){
			$cid=$getCid;
		}else{
			$cid=NULL;
		}

		//组合分类id where条件
		if($cid){
			$where .=' and cid=' . $cid;
		}

		//分页,每页多少条
		$pagerows=isset($attr['pagerows'])?(int)$attr['pagerows']:NULL;
		//可以显示多少页码,默认显示5个页码
		$pagenum=isset($attr['pagenum'])?(int)$attr['pagenum']:5;

		//定义标签tid
		if(isset($attr['tid'])){
			$tid=(int)$attr['tid'];
		}elseif ($getTid=Q('get.tid',0,'intval')) {
			$tid=$getTid;
		}else{
			$tid=NULL;
		}

		//组合标签tid where条件
		if($tid){
			$aidArr=K('ArticleTag')->get_aid($tid);
			if (!empty($aidArr)) {
				$where .=' and aid in('.implode(',', $aidArr). ')';
			}else{
				$where .=' and aid=0';
			}
		}
		//去除左边多余的 and
		$where=preg_replace('/^\s+and\s+/is', '', $where);
		//**************************定义标签属性$attr结束****************************//

		//**************************定义标签内容$content开始*************************//
		//组合返回显示页面的字符串,页面会自动解析
		$str='<?php';
		//如果有分页.//暂时先默认分页样式为2.按理说应该用户指定
		if($pagerows){
			$str .=<<<str
			if(isset(\$_GET['cid'])){
				\$cate=K('Category')->get_one_data(\$_GET['cid']);
				if(\$cate['is_listhtml']){
					Page::\$staticUrl="__ROOT__"."/".STATIC_PATH.\$cate['htmldir']."/{page}.html";
				}
			}
			\$totalRows=K('ArticleView')->where('{$where}')->count();
			\$pageO=new Page(\$totalRows,{$pagerows},{$pagenum},2);
			\$data=K('ArticleView')->where('{$where}')->order({$order})->findAll(\$pageO->limit());
			\$page=\$pageO->show();
			?>
str;
		}else{
			$str.=<<<str
			\$data=K('ArticleView')->limit({$rows})->where('{$where}')->order({$order})->findAll();
			?>
str;
		}
			$str.=<<<str
			<foreach from="\$data" value="\$field">
			<?php
				\$field['thumb']='__ROOT__/'.\$field['thumb'];
				\$field['total']=K('Comment')->total_comment(\$field['aid']);
				\$field['url']=get_arc_url(\$field['is_archtml'],\$field['htmldir'],\$field['aid']);
			?>
			{$content}
			</foreach>
str;
		return $str;
		//**************************定义标签内容$content结束*************************//
	}

	/**
	 * [_channel 定义栏目标签]
	 * @param  [type] $attr    [description]
	 * @param  [type] $content [description]
	 * @param  [type] $parse   [description]
	 * @return [type]          [description]
	 */
	public function _channel($attr,$content,$parse){
		$where='is_show=1';
		//处理用户传递的信息
		$rows= isset($attr['rows'])?$attr['rows']:NULL;
		$order=isset($attr['order'])?$attr['order']:'cid ASC';
		$cid=isset($attr['cid'])?(int)$attr['cid']:NULL;
		$type=isset($attr['type'])?$attr['type']:'self';
		if($cid){
			switch ($type) {
				case 'top': //调顶级栏目
					$pid=K('category')->get_pid($cid);
					$where .=' and cid='.$pid;
					break;
				case 'son': //调子栏目
					$where .=' and pid='.$cid;
					break;
				default:	//默认调同级栏目
					$pid=K('category')->get_pid($cid);
					if($pid) $where .=' and pid='.$pid;
					break;
			}
		}
		$str=<<<str
		<?php
		\$data=K('Category')->where('{$where}')->order('{$order}')->limit($rows)->findAll();
		?>
		<foreach from="\$data" value="\$field">
			<?php
			\$field['url']=get_cate_url(\$field['is_listhtml'],\$field['htmldir'],\$field['cid'])
			?>
			{$content}
		</foreach>
str;
		return $str;
	}
	/**
	 * [_taglist 定义标签云标签]
	 * @param  [type] $attr    [description]
	 * @param  [type] $content [description]
	 * @param  [type] $parse   [description]
	 * @return [type]          [description]
	 */
	public function _taglist($attr,$content,$parse){
		$rows= isset($attr['rows'])?$attr['rows']:NULL;

		$str=<<<str
		<?php
		\$data=K('Tag')->limit('{$rows}')->get_data();
		?>
		<foreach from="\$data" value="\$field">
		<?php 
			\$field['url'] = U('List/index',array('tid'=>\$field['tid']))
		?>
		{$content}
		</foreach>
str;
		return $str;
	}

	/**
	 * [_flink 定义友情链接标签]
	 * @param  [type] $attr    [description]
	 * @param  [type] $content [description]
	 * @param  [type] $parse   [description]
	 * @return [type]          [description]
	 */
	public function _flink($attr,$content,$parse){
		$rows= isset($attr['rows'])?$attr['rows']:NULL;
		$where='is_show=1';
		$order=isset($attr['order'])?$attr['order']:'sort ASC';
		$str=<<<str
		<?php
		\$data=K('Flink')->limit({$rows})->where('{$where}')->order('{$order}')->get_data();
		?>
		<foreach from="\$data" value="\$field">
		<?php 
			\$field['logo']='__ROOT__/'.\$field['logo'];
		?>
		{$content}
		</foreach>
str;
		return $str;
	}

	/**
	 * [_feedback 当前文章的评论标签]
	 * @param  [type] $attr    [description]
	 * @param  [type] $content [description]
	 * @param  [type] $parse   [description]
	 * @return [type]          [description]
	 */
	public function _feedback($attr,$content,$parse){
		//定义调用评论条数
		$rows= isset($attr['rows'])?$attr['rows']:NULL;
		//定义排序
		$order=isset($attr['order'])?".".$attr['order'].".":'"addtime DESC"';
		//获得当前文章的aid
		if(isset($attr['aid'])){
			$aid=(int)$attr['aid'];
		}else if($getaid=Q('get.aid',0,'intval')){
			$aid=$getaid;
		}else{
			$aid=NULL;
		}

		$str=<<<str
		<?php
		\$data=K('Comment')->limit({$rows})->get_arc_comment({$aid},{$order});
		?>
		<foreach from="\$data" value="\$field">
		{$content}
		</foreach>
str;
		return $str;
	}
}
?>