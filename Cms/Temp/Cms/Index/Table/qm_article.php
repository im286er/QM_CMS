<?php if(!defined('HDPHP_PATH'))exit;
return array (
  'aid' => 
  array (
    'field' => 'aid',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => true,
    'default' => NULL,
    'extra' => 'auto_increment',
  ),
  'title' => 
  array (
    'field' => 'title',
    'type' => 'char(120)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'click' => 
  array (
    'field' => 'click',
    'type' => 'mediumint(8) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'sendtime' => 
  array (
    'field' => 'sendtime',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'updatetime' => 
  array (
    'field' => 'updatetime',
    'type' => 'timestamp',
    'null' => 'NO',
    'key' => false,
    'default' => 'CURRENT_TIMESTAMP',
    'extra' => 'on update CURRENT_TIMESTAMP',
  ),
  'thumb' => 
  array (
    'field' => 'thumb',
    'type' => 'varchar(100)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'is_recycle' => 
  array (
    'field' => 'is_recycle',
    'type' => 'tinyint(3) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'digest' => 
  array (
    'field' => 'digest',
    'type' => 'varchar(255)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
  'attr' => 
  array (
    'field' => 'attr',
    'type' => 'set(\'图文\',\'推荐\',\'热门\',\'置顶\')',
    'null' => 'YES',
    'key' => false,
    'default' => NULL,
    'extra' => '',
  ),
  'author' => 
  array (
    'field' => 'author',
    'type' => 'char(15)',
    'null' => 'NO',
    'key' => false,
    'default' => 'admin',
    'extra' => '',
  ),
  'category_cid' => 
  array (
    'field' => 'category_cid',
    'type' => 'smallint(5) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'user_uid' => 
  array (
    'field' => 'user_uid',
    'type' => 'int(10) unsigned',
    'null' => 'NO',
    'key' => false,
    'default' => '0',
    'extra' => '',
  ),
  'source' => 
  array (
    'field' => 'source',
    'type' => 'varchar(30)',
    'null' => 'NO',
    'key' => false,
    'default' => '',
    'extra' => '',
  ),
);
?>