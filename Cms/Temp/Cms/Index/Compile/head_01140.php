<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
    
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>
        鲜果 - 关注你感兴趣的人和事
    </title>
    <meta name="keywords" content="鲜果,鲜果联播,鲜果阅读器,rss阅读器,rss,feed,移动阅读,新闻,资讯">
    <meta name="description" content="鲜果是国内最大的个性化阅读服务提供商，聚合微博、博客、新闻、报刊杂志、免费小说、科技、互联网、体育、娱乐、时尚等精彩内容，为您带来如杂志般赏心悦目的阅读体验。">
    <meta property="qc:admins" content="1163271261601167576375">
    <link rel="alternate" type="application/rss+xml" href="http://feed.feedsky.com/xianguoblog"
    title="鲜果日志">
    <link rel="shortcut icon" type="image/x-icon" href="http://xianguo.com/static/favicon.ico?v6_7.1">
    <link rel="stylesheet" type="text/css" href="http://localhost/HDPHP_CMS/Templates/default2/Css/base.css">
    <link rel="stylesheet" type="text/css" href="http://localhost/HDPHP_CMS/Templates/default2/Css/layout.css">
    <link rel="stylesheet" type="text/css" href="http://localhost/HDPHP_CMS/Templates/default2/Css/common.css">
    <script type="text/javascript" async="" src="http://localhost/HDPHP_CMS/Templates/default2/Js/ga.js">
    </script>
    <script type="text/javascript" src="http://localhost/HDPHP_CMS/Templates/default2/Js/require.js">
    </script>
    <script type="text/javascript" src="http://localhost/HDPHP_CMS/Templates/default2/Js/jquery-1.6.4.min.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="/static/v5/wheels/gotop-btn/main.js" src="http://localhost/HDPHP_CMS/Templates/default2/Js/main.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="$" src="http://localhost/HDPHP_CMS/Templates/default2/Js/jquery.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="ease" src="http://localhost/HDPHP_CMS/Templates/default2/Js/jquery.ui.effect.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="domready" src="http://localhost/HDPHP_CMS/Templates/default2/Js/domready.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="/static/v5/wheels/article/main.js" src="http://localhost/HDPHP_CMS/Templates/default2/Js/main(1).js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="/static/v5/wheels/article/recommend_main.js" src="http://localhost/HDPHP_CMS/Templates/default2/Js/recommend_main.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="/static/v5/wheels/article/template.js" src="http://localhost/HDPHP_CMS/Templates/default2/Js/template.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="_" src="http://localhost/HDPHP_CMS/Templates/default2/Js/underscore.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="mustache" src="http://localhost/HDPHP_CMS/Templates/default2/Js/mustache.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="backbone" src="http://localhost/HDPHP_CMS/Templates/default2/Js/backbone.js">
    </script>
    <script type="text/javascript" charset="utf-8" async="" data-requirecontext="_"
    data-requiremodule="handlebars" src="http://localhost/HDPHP_CMS/Templates/default2/Js/handlebars.min.js">
    </script>
</head>

<body youdao="bind">
    <div class="main-wraper">
        <div class="header nolog-header" id="header">
            <div class="hdr-bar">
                <a href="http://localhost/HDPHP_CMS" class="a v b-logo">
                </a>
                <a href="http://localhost/HDPHP_CMS" class="a first hdr-link cur">
                    首页
                </a>
                		<?php
		$data=K('Category')->where('is_show=1')->order('cid ASC')->limit(7)->findAll();
		?>
		<?php if(is_array($data)):?><?php  foreach($data as $field){ ?>
			<?php
			$field['url']=get_cate_url($field['is_listhtml'],$field['htmldir'],$field['cid'])
			?>
			
	                <a href="<?php echo $field['url'];?>" class="a hdr-link">
	                    <?php echo $field['cname'];?>
	                </a>
                
		<?php }?><?php endif;?>
               <!--  <div class="hdr-account">
                   <ul class="trd-login">
                       <li class="v xianguo">
                           <a href="http://xianguo.com/login?rurl=%2F">
                               登录
                           </a>
                       </li>
                       <li class="v sina">
                           <a href="http://xianguo.com/connect?type=sina" target="_blank">
                               sina
                           </a>
                       </li>
                       <li class="v qq">
                           <a href="http://xianguo.com/connect?type=qq" target="_blank">
                               qq
                           </a>
                       </li>
                   </ul>
                   a href="/register" class="a hdr-link">注册</a>
                   <span class="gap"></span>
                   <a href="/login?rurl=%2F" class="a hdr-link">登录</a
               </div> -->
            </div>
            <div id="header-noti-wrap">
                <div id="header-noti" style="display: none">
                    <div class="noti-b-h">
                    </div>
                    <div class="noti-wrap">
                        <a id="header-noti-close" href="javascript:void(0);">
                            x
                        </a>
                        <div id="header-noti-cnt" class="noti-cnt">
                        </div>
                    </div>
                    <div class="noti-b-h">
                    </div>
                </div>
            </div>
        </div>
        <div class="login-bar">
            <div class="hd">
                <p class="v lt">
                    加入鲜果，订阅新鲜资讯，享受一站式阅读
                </p>
                <!-- <div class="v ct">
                    <a href="http://xianguo.com/connect?type=sina" target="_blank" class="v wbbtn">
                        新浪微博帐号
                    </a>
                    <a href="http://xianguo.com/connect?type=qq" target="_blank" class="v qqbtn">
                        QQ帐号
                    </a>
                </div> -->
                <p class="v rt" style="margin-left:300px">
                    <!-- <span>
                        鲜果帐号
                    </span>
                    <a class="login" href="http://xianguo.com/login">
                        登录
                    </a>
                    /
                    <a class="reg" href="http://xianguo.com/register">
                        注册
                    </a> -->
                </p>
            </div>
        </div>