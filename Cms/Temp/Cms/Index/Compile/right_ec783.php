<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><div class="right">
			<div class='hd_title'>
				<strong>标签云</strong>	
			</div>
			<div id="tagBox" class='tags'>
					<?php
		$data=K('Tag')->limit('10')->get_data();
		?>
		<?php if(is_array($data)):?><?php  foreach($data as $field){ ?>
		<?php $field['url'] = U('List/index',array('tid'=>$field['tid']))?>
		
				<a href="<?php echo $field['url'];?>"><?php echo $field['tagname'];?></a>
			
		<?php }?><?php endif;?>
			</div>
			<div class='hd_title'>
				<strong>热门文章</strong>	
			</div>
			<ul class='arc_list'>
			<?php			$data=K('ArticleView')->limit(4)->where('attr like "%热门%" and cid=5')->order("sendtime DESC")->findAll();
			?>			<?php if(is_array($data)):?><?php  foreach($data as $field){ ?>
			<?php
				$field['thumb']='http://localhost/HDPHP_CMS/'.$field['thumb'];
				$field['total']=K('Comment')->total_comment($field['aid']);
				$field['url']=get_arc_url($field['is_archtml'],$field['htmldir'],$field['aid']);
			?>
			
				<li>
					<div class='image'>
						<a href="<?php echo $field['url'];?>">
							<img src="<?php echo $field['thumb'];?>" alt="<?php echo $field['title'];?>"/>
						</a>
					</div>
					<h2><a href="<?php echo $field['url'];?>"><?php echo $field['title'];?></a></h2>
					<p>
						<em><?php echo date('m-d',$field['sendtime']);?></em><span><?php echo $field['total'];?>条评论</span>
					</p>
				</li>
			
			<?php }?><?php endif;?>
			</ul>
			<div class='hd_title'>
				<strong>推荐文章</strong>	
			</div>
			
			<ul class='arc_list'>
			<?php			$data=K('ArticleView')->limit(2)->where('attr like "%推荐%" and cid=5')->order("sendtime DESC")->findAll();
			?>			<?php if(is_array($data)):?><?php  foreach($data as $field){ ?>
			<?php
				$field['thumb']='http://localhost/HDPHP_CMS/'.$field['thumb'];
				$field['total']=K('Comment')->total_comment($field['aid']);
				$field['url']=get_arc_url($field['is_archtml'],$field['htmldir'],$field['aid']);
			?>
			
				<li>
					<div class='image'>
						<a href="<?php echo $field['url'];?>">
							<img src="<?php echo $field['thumb'];?>" alt="<?php echo $field['title'];?>"/>
						</a>
					</div>
					<h2><a href=""><?php echo $field['title'];?></a></h2>
					<p>
						<em><?php echo date('m-d',$field['sendtime']);?></em><span><?php echo $field['total'];?>条评论</span>
					</p>
				</li>
			
			<?php }?><?php endif;?>
			</ul>
		</div>