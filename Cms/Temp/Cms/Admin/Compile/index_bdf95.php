<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>链接管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Flink&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Flink';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Flink&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Flink';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Flink&m=edit_flink&fid=28';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Flink&m=edit_flink&fid=28';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <style type="text/css">
        /*修改按钮*/
       .table2 tr td{
            height: 35px;
        }
        input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
        input[type="text"]:focus{
            background: none repeat scroll 0 0 #FFFBDE;
            border: 2px solid #51B4FF;
            box-shadow: none !important;
            outline: 1px solid #51B4FF;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public/Css/common.css"/>
    <script type="text/javascript" src="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Flink/js/js.js"></script>
</head>
<body>
<div class="wrap">
    <table class="table2">
        <thead>
        <tr>
            <td width="30">fid</td>
            <td >网站名称</td>
            <td >网站logo</td>
            <td >链接地址</td>
            <td >添加时间</td>
            <td >是否显示</td>
            <td width="80">分类排序</td>
            <td width="200">操作</td>
        </tr>
        </thead>
        <tbody>
        <?php if(is_array($flink)):?><?php  foreach($flink as $value){ ?>
            <tr>
                <td width="30"><?php echo $value['fid'];?></td>
                <td><?php echo $value['fname'];?></td>
                <td><img src="http://localhost/HDPHP_CMS/<?php echo $value['logo'];?>" alt="无logo图片" style="width:35px;height:35px;"/></td>
                <td><?php echo $value['url'];?></td>
                <td><?php echo $value['addtime'];?></td>
                <td>
                    <?php if($value['is_show'] == 0){?>
                        <a href="javascript:" class="glyphicon glyphicon-remove" onclick="hd_ajax('<?php echo U('Flink/ajax_update_stat',array('fid'=>$value['fid'],'switch'=>1));?>')"></a>
                    <?php  }else{ ?>
                        <a href="javascript:" class="glyphicon glyphicon-ok" onclick="hd_ajax('<?php echo U('Flink/ajax_update_stat',array('fid'=>$value['fid'],'switch'=>0));?>')"></a>
                    <?php }?>
                </td>
                <td><?php echo $value['sort'];?></td>
                <td width="200">
                    <a href="<?php echo U('edit_flink',array('fid'=>$value['fid']));?>" class="btn btn-info btn-xs edit_tag">修改</a>
                    <a href="javascript:" class="btn btn-warning btn-xs" onclick="if(confirm('确定要删除吗？')) hd_ajax('<?php echo U('del_flink',array('fid'=>$value['fid']));?>')">删除</a>
                </td>
            </tr>
        <?php }?><?php endif;?>
        </tbody>
    </table>
    <div class="page_hdjob">
        
    </div>
</div>

</body>
</html>


