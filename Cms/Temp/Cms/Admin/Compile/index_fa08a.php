<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>评论管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Comment&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Comment';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Comment&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Comment';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public/Css/common.css"/>
</head>
<body>
<div class="wrap">
    <table class="table2">
        <thead>
            <tr>
                <td width="50">coid</td>
                <td width="50">pid</td>
                <td width="100">评论用户名/昵称</td>
                <td width="180">评论时间</td>
                <td width="180">评论内容</td>
                <td width="50">所属文章</td>
                <td width="150">操作</td>
            </tr>
        </thead>
        <tbody>
            <?php if(is_array($comment)):?><?php  foreach($comment as $value){ ?>
                <tr>
                    <td width="50"><?php echo $value['coid'];?></td>
                    <td width="50"><?php echo $value['pid'];?></td>
                    <td width="100"><?php echo $value['username'];?>/<?php echo $value['nickname'];?></td>
                    <td width="180"><?php echo hd_date($value['addtime'],'Y-m-d H:i:s');?></td>
                    <td width="180"><?php echo $value['comcon'];?></td>
                    <td width="50"><?php echo $value['article_aid'];?></td>
                    <td width="150">
                         <a href="javascript:" class="btn-sm btn-warning" onclick="if(confirm('确定要删除吗？')) hd_ajax('<?php echo U('del_comment',array('coid'=>$value['coid']));?>')">删除</a>
                    </td>
                </tr>
            <?php }?><?php endif;?>
        </tbody>
    </table>
    <div class="page_hdjob">
      
    </div>
</div>

</body>
</html>


