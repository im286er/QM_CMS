<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>分类管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Category';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <style type="text/css">
        .plus_minus{
            width: 20px;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Category/css/css.css"/>
    <script type="text/javascript" src="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Category/js/js.js"></script>
</head>
<body>
<div class="wrap">
    <table class="table2">
        <thead>
        <tr pid=0>
            <td width="30"></td>
            <td>CID</td>
            <td>分类名称</td>
            <td>分类标题</td>
            <td>分类排序</td>
            <td>静态目录</td>
            <td>列表静态</td>
            <td>内容静态</td>
            <td>是否显示</td>
            <td width="200">操作</td>
        </tr>
        </thead>
        <tbody>
            <?php if(is_array($data)):?><?php  foreach($data as $v){ ?>
                <tr style="height:30px" cid=<?php echo $v['cid'];?> pid=<?php echo $v['pid'];?>>
                    <td width="80" >
                        <?php echo $v['_html'];?><span class="btn btn-success btn-xs plus_minus">+</span>
                    </td>
                    <td><?php echo $v['cid'];?></td>
                    <td><?php echo $v['cname'];?></td>
                    <td><?php echo $v['ctitle'];?></td>
                    <td><?php echo $v['csort'];?></td>
                    <td><?php echo $v['htmldir'];?></td>
                    <td>
                        <?php if($v['is_listhtml'] == 0){?>
                            <a href="javascript:" class="glyphicon glyphicon-remove" onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'list','switch'=>1));?>')"></a>
                        <?php  }else{ ?>
                            <a href="javascript:" class="glyphicon glyphicon-ok"  onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'list','switch'=>0));?>')"></a>
                        <?php }?>
                    </td>
                    <td>
                        <?php if($v['is_archtml'] == 0){?>
                            <a href="javascript:" class="glyphicon glyphicon-remove" onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'arc','switch'=>1));?>')"></a>
                        <?php  }else{ ?>
                            <a href="javascript:" class="glyphicon glyphicon-ok"  onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'arc','switch'=>0));?>')"></a>
                        <?php }?>
                    </td>
                    <td>
                         <?php if($v['is_show'] == 0){?>
                            <a href="javascript:" class="glyphicon glyphicon-remove" onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'show','switch'=>1));?>')"></a>
                        <?php  }else{ ?>
                            <a href="javascript:" class="glyphicon glyphicon-ok" onclick="hd_ajax('<?php echo U('Category/ajax_update_stat',array('cid'=>$v['cid'],'type'=>'show','switch'=>0));?>')"></a>
                        <?php }?>
                    </td>
                    <td width="160" class="btn-group btn-group-xs">
                        <a href="<?php echo U('edit',array('cid'=>$v['cid']));?>" class="btn btn-default">编辑</a>
                        <a href="<?php echo U('add',array('cid'=>$v['cid']));?>" class="btn btn-default">添加子分类</a>
                        <a href="javascript:" class="btn btn-default" onclick="if(confirm('确定要删除吗？')) hd_ajax('<?php echo U('del',array('cid'=>$v['cid']));?>')">删除</a>
                    </td>
                </tr>
            <?php }?><?php endif;?>
        </tbody>
    </table>
    <div class="page_hdjob">
        <?php echo $page;?>
    </div>
</div>

</body>
</html>


