<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>回收站</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=recycle';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=recycle';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Article';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public/Css/common.css"/>
</head>
<body>
<div class="wrap">
    <table class="table2">
        <thead>
        <tr>
            <td width="50">aid</td>
            <td>文章标题</td>
            <td width="100">属性</td>
            <td width="180">添加时间</td>
            <td width="180">修改时间</td>
            <td width="130">所属分类<a href="<?php echo U('recycle');?>">[显示全部]</a></td>
            <td width="50">作者</td>
            <td width="150">操作</td>
        </tr>
        </thead>
        <tbody>
            <?php if(is_array($bin)):?><?php  foreach($bin as $value){ ?>
                <tr>
                    <td width="50"><?php echo $value['aid'];?></td>
                    <td><a href="" target="_blank"><?php echo $value['title'];?></a></td>
                    <td width="100"><?php echo $value['attr'];?></td>
                    <td width="180"><?php echo hd_date($value['sendtime'],'Y-m-d H:i:s');?></td>
                    <td width="180"><?php echo $value['updatetime'];?></td>
                    <td width="100"><a href="<?php echo U('recycle',array('cid'=>$value['cid']));?>"><?php echo $value['cname'];?></a></td>
                    <td width="50"><?php echo $value['author'];?></td>
                    <td width="150">
                        <a href="javascript:" class="btn-sm btn-info" onclick="hd_ajax('<?php echo U('recovery',array('aid'=>$value['aid']));?>')">还原</a>
                        <a href="javascript:" class="btn-sm btn-warning" onclick="if(confirm('删除后就无法恢复了,您确定要删除吗？')) hd_ajax('<?php echo U('del',array('aid'=>$value['aid']));?>')">彻底删除</a>
                    </td>
                </tr>
            <?php }?><?php endif;?>
        </tbody>
    </table>
    <div class="page_hdjob">
        
    </div>
</div>

</body>
</html>


