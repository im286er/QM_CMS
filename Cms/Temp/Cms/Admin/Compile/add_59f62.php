<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>分类管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category&m=add';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category&m=add';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Category';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Category/css/css.css"/>
</head>
<body>
<div class="wrap">
    <div class="title-header">添加分类</div>
    <form action="" method="post" class="form-inline hd-form" onsubmit="return hd_submit(this,'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Category&m=index')">
        <table class="table1">
            <tr>
                <th class="w100">分类名称</th>
                <td  class="w100">
                    <input type="text" name="cname" class="w200"/> 
                </td>
                <td>分类名称长度 1 到 20 位</td>
            </tr>
            <tr>
                <th class="w100">所属分类</th>
                <td>
                    <?php if(isset($_GET['cid'])){?>
                        <span><?php echo $data['cname'];?></span>
                    <?php  }else{ ?>
                        <span>顶级分类</span>
                    <?php }?>

                    <!-- <select name="pid" class="w200">
                        <option value="0">顶级分类</option>
                    </select> -->
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">分类标题</th>
                <td>
                    <input type="text" name="ctitle" class="w200"/>
                </td>
                <td> 分类标题长度 1 到 60 位 </td>
            </tr>
            <tr>
                <th class="w100">分类关键字</th>
                <td>
                    <textarea name="ckeywords" class="form-control" rows="5"></textarea>
                </td>
                <td>分类关键字长度 1 到 255位</td>
            </tr>
            <tr>
                <th class="w100">分类描述</th>
                <td>
                    <textarea name="cdes" class="form-control" rows="5"></textarea>
                </td>
                <td>分类描述长度 1 到 255位  </td>
            </tr>
            <tr>
                <th class="w100">分类排序</th>
                <td>
                    <input type="text" name="csort" class="w200" value="100"/>
                </td>
                <td>请填写1到65535的排序数字 </td>
            </tr>
            <tr>
                <th class="w100">静态目录</th>
                <td>
                    <input type="text" name="htmldir" class="w200"/>
                </td>
                <td>请填写1~20位只包含字母数字下划线,分类静态目录 例如：php</td>
            </tr>
             <tr>
                <th class="w100">列表页静态</th>
                <td>
                    是：<input type="radio" name="is_listhtml" id="" value="1"/>
                    &nbsp;
                    否: <input type="radio" name="is_listhtml" id="" checked="checked" value="0"/>
                </td>
                <td></td>
            </tr>
             <tr>
                <th class="w100">内容页静态</th>
                <td>
                    是：<input type="radio" name="is_archtml" id="" value="1"/>
                    &nbsp;
                    否: <input type="radio" name="is_archtml" id="" checked="checked" value="0"/>
                </td>
                <td></td>
            </tr>
             <tr>
                <th class="w100">是否显示</th>
                <td>
                    是：<input type="radio" name="is_show" id="" checked="checked" value="1"/>
                    &nbsp;
                    否: <input type="radio" name="is_show" id=""  value="0"/>
                </td>
                <td></td>
            </tr>     
        </table>
        <div class="position-bottom">
            <?php if(isset($_GET['cid'])){?>
                <input type="hidden" name="pid" value="<?php echo $_GET['cid'];?>"/>
            <?php }?>
            <input type="submit" class="hd-success" value="添加"/>
        </div>
    </form>
</div>
</body>
</html>