<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>修改文章</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=edit&aid=10';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=edit';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Article';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Article/css/css.css"/>
    <style type="text/css">
        .checkbox{
             margin-left:5px;
             width:90px;
             margin-top: 10px;
        }
        textarea{
            width: 300px;
            height: 100px;
        }
    </style>
</head>
<body>
<div class="wrap">
    <div class="title-header">修改文章</div>
    <form action="" method="post" class="form-inline hd-form" enctype="multipart/form-data">
        <table class="table1">
            <tr>
                <th class="w10">文章标题</th>
                <td class="w100">
                    <input type="text" name="title" class="w300" value='<?php echo $article['title'];?>'/> 
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">作者</th>
                <td>
                    <input type="text" name="author" class="w200" value="<?php echo $article['author'];?>"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">来源</th>
                <td>
                    <input type="text" name="source" class="w200" value="<?php echo $article['source'];?>"/>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">所属分类</th>
                <td>
                    <select name="category_cid" class="w200">
                        <option value="<?php echo $article['cid'];?>"><?php echo $article['cname'];?></option>
                        <?php if(is_array($category)):?><?php  foreach($category as $value){ ?>
                            <option value="<?php echo $value['cid'];?>"><?php echo $value['_html'];?><?php echo $value['cname'];?></option>
                        <?php }?><?php endif;?>
                    </select>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">缩略图</th>
                <td>
                     <input type="file" name="upload" id="" />
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">文章属性</th>
                <td>
                    <label class="checkbox-inline">
                        热门
                        <input type="checkbox" value="热门" name="attr[]" class="check_input" 
                            <?php if(in_array('热门',$article['attr'])){?>checked="checked"<?php }?>
                        >
                    </label>
                    <label class="checkbox-inline">
                        置顶
                        <input type="checkbox" value="置顶" name="attr[]" class="check_input"
                            <?php if(in_array('置顶',$article['attr'])){?>checked="checked"<?php }?>
                        >
                    </label>
                    <label class="checkbox-inline">
                        推荐
                        <input type="checkbox" value="推荐" name="attr[]" class="check_input"
                            <?php if(in_array('推荐',$article['attr'])){?>checked="checked"<?php }?>
                        >
                    </label>
                    <label class="checkbox-inline">
                        图文
                        <input type="checkbox" value="图文" name="attr[]" class="check_input"
                            <?php if(in_array('图文',$article['attr'])){?>checked="checked"<?php }?>
                        >
                    </label>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">文章标签</th>
                <td>
                    <?php if(is_array($tag)):?><?php  foreach($tag as $value){ ?>
                        <label class="checkbox-inline">
                            <?php echo $value['tagname'];?>
                            <input type="checkbox" value="<?php echo $value['tid'];?>" name="tag_tid[]" class="check_input"
                            <?php if(in_array($value['tid'],$tag_tids)){?>checked="checked"<?php }?>
                            >
                        </label>
                    <?php }?><?php endif;?>
                </td>
                <td></td>
            </tr>
             <tr>
                <th class="w100">文章关键字</th>
                <td>
                    <textarea name="keywords" id="" rows="10"><?php echo $article['keywords'];?></textarea>
                </td>
                <td></td>
            </tr>
             <tr>
                <th class="w100">文章描述</th>
                <td>
                    <textarea name="description" id="" cols="30" rows="10"><?php echo $article['description'];?></textarea>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100">文章摘要</th>
                <td>
                    <script charset="utf-8" src="http://localhost/hdphp/hdphp/Extend/Org/Keditor/kindeditor-all-min.js"></script>
            <script charset="utf-8" src="http://localhost/hdphp/hdphp/Extend/Org/Keditor/lang/zh_CN.js"></script>
        <textarea id="hd_digest" name="digest"><?php echo $article['digest'];?></textarea>
    <script>
        var options_digest = {
        filterMode : false
                ,id : "editor_id"
        ,width : "300px"
        ,height:"100px"
                ,formatUploadUrl:false
        ,allowFileManager:false
        ,allowImageUpload:true
        ,uploadJson : "http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=keditor_upload&editor_type=2&image=0&uploadsize=2000000&maximagewidth=false&maximageheight=false&hd_sid=afs538gga2b5d7jr5jvsc0ud86"//处理上传脚本
        };options_digest.items=[
            "source","code","image","fullscreen","|","forecolor", "bold", "italic", "underline",
            "removeformat", "|", "justifyleft", "justifycenter", "justifyright", "insertorderedlist",
            "insertunorderedlist", "|", "emoticons",  "link"];var hd_digest;
        KindEditor.ready(function(K) {
                    hd_digest = KindEditor.create("#hd_digest",options_digest);
        });
        </script>
        
                </td>
                <td></td>
            </tr>
             <tr>
                <th class="w100">文章正文</th>
                <td>
                    <script type="text/javascript" charset="utf-8" src="http://localhost/hdphp/hdphp/Extend/Org/Ueditor/ueditor.config.js"></script><script type="text/javascript" charset="utf-8" src="http://localhost/hdphp/hdphp/Extend/Org/Ueditor/ueditor.all.min.js"></script><script type="text/javascript">UEDITOR_HOME_URL="http://localhost/hdphp/hdphp/Extend/Org/Ueditor/"</script><script id="hd_content" name="content" type="text/plain"><?php echo $article['content'];?></script>
        <script type='text/javascript'>
        $(function(){
                var ue = UE.getEditor('hd_content',{
                imageUrl:'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Article&m=ueditor_upload&water=0&uploadsize=2000000&maximagewidth=false&maximageheight=false'//处理上传脚本
                ,zIndex : 0
                ,autoClearinitialContent:false
                ,initialFrameWidth:"850" //宽度1000
                ,initialFrameHeight:"300" //宽度1000
                ,autoHeightEnabled:false //是否自动长高,默认true
                ,autoFloatEnabled:false //是否保持toolbar的位置不动,默认true
                ,maximumWords:2000 //允许的最大字符数
                ,readonly : false //编辑器初始化结束后,编辑区域是否是只读的，默认是false
                ,wordCount:true //是否开启字数统计
                ,imagePath:''//图片修正地址
                , toolbars:[
            ['fullscreen', 'source', '|', 'undo', 'redo', '|',
                'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'superscript', 'subscript', 'removeformat', 'formatmatch', 'autotypeset', 'blockquote', 'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', 'cleardoc', '|',
                'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                'customstyle', 'paragraph', 'fontfamily', 'fontsize', '|',
                'directionalityltr', 'directionalityrtl', 'indent', '|',
                'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|', 'touppercase', 'tolowercase', '|',
                'link', 'unlink', 'anchor', '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                'insertimage', 'emotion', 'scrawl', 'insertvideo', 'music', 'attachment', 'map', 'gmap', 'insertframe','insertcode', 'pagebreak', 'template', 'background', '|',
                'horizontal', 'date', 'time', 'spechars', 'snapscreen', 'wordimage', '|',
                'inserttable', 'deletetable', 'insertparagraphbeforetable', 'insertrow', 'deleterow', 'insertcol', 'deletecol', 'mergecells', 'mergeright', 'mergedown', 'splittocells', 'splittorows', 'splittocols', 'charts', '|',
                'print', 'preview', 'searchreplace', 'drafts']
            ]//工具按钮
                , initialStyle:'p{line-height:1em; font-size: 14px; }'
            });
        })
        </script>
                </td>
                <td></td>
            </tr>
            <tr>
                <th class="w100"></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th class="w100"></th>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <th class="w100"></th>
                <td></td>
                <td></td>
            </tr>
        </table>
        <div class="position-bottom" style='padding-left:125px'>
            <a href="<?php echo U('index');?>"><input type="button" class="hd-success btn-sm" value="取消修改"/></a>
            <input type="hidden" value="<?php echo $_GET['aid'];?>" name="aid"/>
            <input type="submit" class="hd-success btn-sm" value="确认修改"/>
        </div>
    </form>
</div>
</body>
</html>