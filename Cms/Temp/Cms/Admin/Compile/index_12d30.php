<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>备份管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=BackUp&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=BackUp';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=BackUp&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/BackUp';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <!-- <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/Static/css/common.css"/> -->
</head>
<body>
<div class="wrap">
	<form action="<?php echo U('Admin/BackUp/recovery');?>" method="post">
    <table class="table2">
        <thead>
        <tr>
            <td width="150">备份时间</td>
            <td>备份目录</td>
            <td width="100">选择</td>
            <td width="100">删除</td>
        </tr>
        </thead>
        <tbody>
            <?php if(is_array($dir)):?><?php  foreach($dir as $v){ ?>
        	   <tr>
                    <td width="50"><?php echo date('Y-m-d H:i:s',$v['fileatime']);?></td>
                    <td><?php echo $v['path'];?></td>
                    <td width="100"><input type="radio" name="dir" value="<?php echo $v['name'];?>" /></td>
                    <td width="100">
                        <a href="javascript:" onclick="if(confirm('删除后就无法恢复,确定要删除吗？')) hd_ajax('<?php echo U('Admin/BackUp/del_data',array('dir'=>$v['path']));?>')"><img src="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/BackUp/images/116.png" alt="删除" class="img-circle" style="width:28px;height:28px;"></a>
                    </td>
                </tr>
            <?php }?><?php endif;?>
        </tbody>
    </table>
    <?php if(!empty($dir)){?>
    <div style="margin-top:20px;margin-left:20px;">
         <!-- <a href="javascript:" onclick="if(confirm('确定要删除吗？')) hd_ajax('<?php echo U('Admin/BackUp/del_data',array('aid'=>3));?>')"><input type="button" value="删除" class="hd-cancel"/></a> -->
        <input type="submit" value="还原" class="hd-success"/>
    </div>
    <?php }?>
    </form>
</div>

</body>
</html>