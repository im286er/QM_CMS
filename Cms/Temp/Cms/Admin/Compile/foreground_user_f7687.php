<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
	<title></title>
	<script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=User&m=foreground_user';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=User';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=User&m=foreground_user';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/User';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Index&m=index';
</script>
	<link rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/User/Css/public.css" />
	<script type="text/javascript" src="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/User/Js/public.js"></script>	
</head>

<body>
	<table class="table">
		<tr>
			<td class="th" colspan="20">前台用户列表</td>
		</tr>
		<tr class="tableTop">
			<td class="tdLittle1">ID</td>
			<td>用户名</td>
			<td>昵称</td>
			<td>帐号状态</td>
			<td>锁定</td>
			<td>操作</td>
		</tr>
		<?php if(is_array($foreground)):?><?php  foreach($foreground as $value){ ?>
		<tr>
			<td><?php echo $value['uid'];?></td>
			<td><?php echo $value['username'];?></td>
			<td><?php echo $value['nickname'];?></td>
			<?php if($value['is_lock'] == 1){?>	
				<td>锁定</td>	
			<?php  }else{ ?>
				<td>正常</td>
			<?php }?>
			<td>
			<?php if($value['is_lock'] == 1){?>
				<a href="<?php echo U('unlock_user',array('uid'=>$value['uid']));?>">解锁</a>
			<?php  }else{ ?>
				<a href="<?php echo U('lock_user',array('uid'=>$value['uid']));?>">锁定</a>
			<?php }?>
			</td>
			<td>
				<a href="javascript:" onclick="if(confirm('确定要删除吗？')) hd_ajax('<?php echo U('del_user',array('uid'=>$value['uid']));?>')">删除</a>
			</td>
		</tr>
		<?php }?><?php endif;?>
	</table>

</body>
</html>