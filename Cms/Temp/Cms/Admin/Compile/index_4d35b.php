<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>站点配置项</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=WebSet&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=WebSet';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=WebSet&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/WebSet';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <!-- <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/WebSet/css/css.css"/> -->
</head>
<body>
<div class="wrap">
    <div class="title-header">站点配置</div>
    <form action="" method="post" class="form-inline hd-form" onsubmit="return hd_submit(this)">
        <table class="table1">
            <tr>
                <th colspan="100">站点信息</th>
            </tr>
            <tr>
                <td class="w100">名称</td>
                <td class="w100">值</td>
                <td class="w150">标题</td>
                <td class="w150">描述</td>
            </tr>
            <?php if(is_array($data[1])):?><?php  foreach($data[1] as $value){ ?>
                <tr>
                    <td class="w100"><?php echo $value['name'];?></td>
                    <td class="w100">
                        <?php if($value['name'] == 'index_tpl_style'){?>
                            <select name="coid[<?php echo $value['coid'];?>]" class="w150">
                                <?php if(is_array($dir)):?><?php  foreach($dir as $v){ ?>  
                                    <option value="<?php echo $v['name'];?>" <?php if($value['value'] == $v['name']){?>selected="selected"<?php }?>><?php echo $v['name'];?></option>
                                <?php }?><?php endif;?>
                            </select>
                        <?php  }else{ ?>
                            <input type="text" value="<?php echo $value['value'];?>" name="coid[<?php echo $value['coid'];?>]"/>
                        <?php }?>
                    </td>
                    <td class="w150"><?php echo $value['title'];?></td>
                    <td class="w150"><?php echo $value['des'];?></td>
                </tr>
            <?php }?><?php endif;?>
            <tr>
                <th colspan="100">验证码配置</th>
            </tr>
            <tr>
                <td class="w100">名称</td>
                <td class="w100">值</td>
                <td class="w150">标题</td>
                <td class="w150">描述</td>
            </tr>
            <?php if(is_array($data[2])):?><?php  foreach($data[2] as $value){ ?>
                <tr>
                    <td class="w100"><?php echo $value['name'];?></td>
                    <td class="w100">
                        <input type="text" value="<?php echo $value['value'];?>" name="coid[<?php echo $value['coid'];?>]"/>
                    </td>
                    <td class="w150"><?php echo $value['title'];?></td>
                    <td class="w150"><?php echo $value['des'];?></td>
                </tr>
            <?php }?><?php endif;?>
            <tr>
                <th colspan="100">水印配置</th>
            </tr>
            <tr>
                <td class="w100">名称</td>
                <td class="w100">值</td>
                <td class="w150">标题</td>
                <td class="w150">描述</td>
            </tr>
            <?php if(is_array($data[3])):?><?php  foreach($data[3] as $value){ ?>
                <tr>
                    <td class="w100"><?php echo $value['name'];?></td>
                    <td class="w100">
                        <input type="text" value="<?php echo $value['value'];?>" name="coid[<?php echo $value['coid'];?>]"/>
                    </td>
                    <td class="w150"><?php echo $value['title'];?></td>
                    <td class="w150"><?php echo $value['des'];?></td>
                </tr>
            <?php }?><?php endif;?>
            <tr>
                <th colspan="100">缩略图配置</th>
            </tr>
            <tr>
                <td class="w100">名称</td>
                <td class="w100">值</td>
                <td class="w150">标题</td>
                <td class="w150">描述</td>
            </tr>
            <?php if(is_array($data[4])):?><?php  foreach($data[4] as $value){ ?>
                <tr>
                    <td class="w100"><?php echo $value['name'];?></td>
                    <td class="w100">
                        <input type="text" value="<?php echo $value['value'];?>" name="coid[<?php echo $value['coid'];?>]"/>
                    </td>
                    <td class="w150"><?php echo $value['title'];?></td>
                    <td class="w150"><?php echo $value['des'];?></td>
                </tr>
            <?php }?><?php endif;?>

        </table>
        <div class="position-bottom">
            <input type="submit" class="hd-success" value="保存"/>
        </div>
    </form>
</div>
</body>
</html>