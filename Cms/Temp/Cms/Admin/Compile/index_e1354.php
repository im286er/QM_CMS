<?php if(!defined("HDPHP_PATH"))exit;C("SHOW_NOTICE",FALSE);?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html;charset=UTF-8"/>
    <title>标签管理</title>
    <script type='text/javascript' src='http://localhost/hdphp/hdphp/../hdjs/jquery-1.8.2.min.js'></script>
<link href='http://localhost/hdphp/hdphp/../hdjs/css/hdjs.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/hdjs.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/js/slide.js'></script>
<script src='http://localhost/hdphp/hdphp/../hdjs/org/cal/lhgcalendar.min.js'></script>
<script type='text/javascript'>
		HOST = 'http://localhost';
		ROOT = 'http://localhost/HDPHP_CMS';
		WEB = 'http://localhost/HDPHP_CMS/index.php';
		URL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Tag&m=index';
		HDPHP = 'http://localhost/hdphp/hdphp';
		HDPHPDATA = 'http://localhost/hdphp/hdphp/Data';
		HDPHPTPL = 'http://localhost/hdphp/hdphp/Lib/Tpl';
		HDPHPEXTEND = 'http://localhost/hdphp/hdphp/Extend';
		APP = 'http://localhost/HDPHP_CMS/index.php?a=Admin';
		CONTROL = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Tag';
		METH = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Tag&m=index';
		GROUP = 'http://localhost/HDPHP_CMS/./Cms';
		TPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl';
		CONTROLTPL = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Tag';
		STATIC = 'http://localhost/HDPHP_CMS/Static';
		PUBLIC = 'http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public';
		HISTORY = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Tag&m=add_tag';
		HTTPREFERER = 'http://localhost/HDPHP_CMS/index.php?a=Admin&c=Tag&m=add_tag';
</script>
    <link href='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/css/bootstrap.min.css' rel='stylesheet' media='screen'>
<script src='http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/bootstrap.min.js'></script>
                <!--[if lte IE 6]>
                <link rel="stylesheet" type="text/css" href="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/ie6/css/bootstrap-ie6.css">
                <![endif]-->
                <!--[if lt IE 9]>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/html5shiv.min.js"></script>
                <script src="http://localhost/hdphp/hdphp/Extend/Org/bootstrap/js/respond.min.js"></script>
                <![endif]-->
    <style type="text/css">
        /*修改按钮*/
       .table2 tr td{
            height: 35px;
        }
        input[type="text"]{
            background-color: #FFFFFF;
            border: 2px solid #DDDDDD;
            border-radius: 0;
            box-shadow: 2px 2px 2px #F0F0F0 inset !important;
            color: #555555;
            display: inline-block;
            font-size: 12px;
            line-height: 30px;
            margin: 0;
            padding: 0 6px;
            vertical-align: middle;
            height: 30px;
        }
        input[type="text"]:focus{
            background: none repeat scroll 0 0 #FFFBDE;
            border: 2px solid #51B4FF;
            box-shadow: none !important;
            outline: 1px solid #51B4FF;
        }
    </style>
    <link type="text/css" rel="stylesheet" href="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Public/Css/common.css"/>
    <script type="text/javascript" src="http://localhost/HDPHP_CMS/./Cms/Cms/Admin/Tpl/Tag/js/js.js"></script>
</head>
<body>
<div class="wrap">
    <table class="table2">
        <thead>
        <tr>
            <td width="30">id</td>
            <td>标签名</td>
            <td width="200">操作</td>
        </tr>
        </thead>
        <tbody>
        <?php if(is_array($data)):?><?php  foreach($data as $value){ ?>
            <tr>
                <td width="30"><?php echo $value['tid'];?></td>
                <td>
                    <span><?php echo $value['tagname'];?></span>
                    <form action="" onsubmit="return hd_submit(this)">
                        <input type="hidden" name="tid" value="<?php echo $value['tid'];?>"/>
                        <input type="text" name="tagname" value="<?php echo $value['tagname'];?>" style="display:none"/>
                    </form>
                </td>
                <td width="200">
                    <a href="javascript:" class="btn btn-info btn-xs edit_tag">编辑</a>
                    <a href="<?php echo U('del_tag',array('tid'=>$value['tid']));?>" class="btn btn-warning btn-xs">删除</a>
                </td>
            </tr>
        <?php }?><?php endif;?>
        </tbody>
    </table>
    <div class="page_hdjob">
        
    </div>
</div>

</body>
</html>


