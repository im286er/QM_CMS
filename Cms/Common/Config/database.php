<?php 
if(!defined('HDPHP_PATH'))exit('No direct script access allowed');
return array (
  'DB_HOST' => '127.0.0.1',
  'DB_USER' => 'root',
  'DB_PASSWORD' => '',
  'DB_DATABASE' => 'qm_cms',
  'DB_PORT' => '3306',
  'DB_PREFIX' => 'qm_',
  'DB_DRIVER' => 'mysqli',
);
