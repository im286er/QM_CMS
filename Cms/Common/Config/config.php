<?php
if (!defined("HDPHP_PATH"))exit('No direct script access allowed');
//更多配置请查看hdphp/Config/config.php
$config = array(

	'URL_TYPE'                      => 2,           //类型 1:PATHINFO模式 2:普通模式 3:兼容模式
	'DB_BACKUP'                     => './Backup/'.date('Ymdhis'),   //数据库备份目录
	
);
return array_merge($config,include COMMON_CONFIG_PATH."webconfig.php",include COMMON_CONFIG_PATH.'database.php');
?>