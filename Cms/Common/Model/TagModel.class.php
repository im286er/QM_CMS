<?php
/* 
* @Title:  [标签模型类]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-20 17:01:40
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-22 18:24:52
* @Copyright:  [hn7m.com]
*/
class TagModel extends Model{
	public $table='tag';

	public $validate = array(
		array('tagname','nonull','标签名不能为空',2,3),
		);

	public function add_data(){
		$tag=Q('post.tagname');

		$preg="/[\s,\r\n]+/is";
		if (!preg_match($preg, $tag)) {
			$this->error='格式不正确,请参考右边规则';
			return false;
		}

		$arr=preg_split($preg, $tag);
		//p($arr);die();
		//去重
		$arr=array_unique($arr);

		//循环插入
		foreach ($arr as $key => $value) {
			if(empty($value)) continue;
			$data=array('tagname'=>$value);
			$this->add($data);
		}
		return true;
	}

	/**
	 * [get_data 获得所有的数据]
	 * @param  [type] $field [description]
	 * @return [type]        [description]
	 */
	public function get_data($field=NULL){
		return $this->field($field)->order('tid asc')->findAll();
	}

	public function del_data($tid){
		// 删除标签数据
		$this->delete($tid);
		// 删除中间表数据
		K('ArticleTag')->del_data($tid);

		return true;
	}

	/**
	 * [update_data 修改]
	 * @return [type] [description]
	 */
	public function update_data(){
		$this->update();
	}
	
}
?>