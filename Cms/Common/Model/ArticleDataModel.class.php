<?php
/* 
* @Title:  [文章数据模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-22 12:23:26
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-22 21:26:40
* @Copyright:  [hn7m.com]
*/
/**
* 	
*/
class ArticleDataModel extends Model
{
	public $table="article_data";
	//验证规则
	public $validate = array(
		array('content','nonull','文章内容不能为空',2,3)
		);
	
}
?>