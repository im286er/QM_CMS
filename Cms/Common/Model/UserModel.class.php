<?php
/* 
* @Title:  [建立用户管理模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-18 16:24:49
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 20:41:27
* @Copyright:  [hn7m.com]
*/

class UserModel extends Model{
	//定义表名
	pubLic $table='user';

	//定义验证规则
	public $validate = array(
		array('username','nonull','用户名不能为空',2,3),
		array('password','nonull','密码不能为空',2,3)
		//以上代码是对用户表单 username 进行验证，验证规则是不为空，如果验证不通过提示信息为 ' 用户名不能为空 '，第 4 个参数为 2 表示必须进行验证,第5个参数为3表示验证时机为任何动作都进行验证 
		);

	/**
	 * [validate_login 验证登录]
	 * @return [type] [description]
	 */
	public function validate_login($admin=TRUE){
		//create方法，
		//执行完他模型里面的自动验证和自动完成才能生效
		if(!$this->create()){
			return false;
		}
		if($admin){
			//比对验证码,1.接收的name 2,如果不存在给默认值 3.自定义函数
			if(Q('post.code', '', 'strtoupper') != session('code')){
				$this->error = '验证不正确';
				return false;
			}
		}
		

		$username = Q('post.username');
		$password = Q('post.password', '', 'md5');

		$where = array(
			'username'	=>	$username,
			'is_admin'	=>	1
			);
		//如果此方法传递false则是验证前台，所以不需要is_admin
		if(!$admin) unset($where['is_admin']);

		$userData = $this->where($where)->find();

		if(!$userData || $password != $userData['password']){
			$this->error = '用户名或者密码不正确';
			return false;
		}
		//是否锁定
		if($userData['is_lock']){
			$this->error = '用户被锁定，请联系管理员';
			return false;
		}

		//走到下面证明全部通过，返回用户所有信息
		return $userData;

	}

	/**
	 * [check_user 检测用户是否存在]
	 * @param  [type] $username [description]
	 * @return [type]           [description]
	 */
	public function check_user($username){
		return $this->where(array('username'=>$username))->getField('uid');
	}

	/**
	 * [add_user 添加用户]
	 * @param [type] $data [description]
	 */
	public function add_user($data=NULL){
		$this->data['password']=Q('password','','md5');
		return $this->add($data);
	}

	/**
	 * [validate_reg PHP注册验证]
	 * @return [type] [description]
	 */
	public function validate_reg(){
		$username = Q('post.username_reg');
		if(!$username){
			$this->error = '用户名不能为空';
			return false;
		}

		$preg = "/^[a-z]{1}\w{4,19}$/is";
		if(!preg_match($preg, $username)){
			$this->error = '用户名格式不正确且必须字母开头为5-20个字符';
			return false;
		}

		$nickname = Q('post.nickname');
		if(mb_strlen($nickname) < 5 || mb_strlen($nickname) > 10){
			$this->error = '昵称必须为5-10个字符';
			return false;
		}

		$password1 = Q('post.password_reg1');
		$password2 = Q('post.password_reg2');

		$preg = "/^\w{6,16}$/is";
		if(!preg_match($preg, $password1)){
			$this->error = '密码必须字母数字下划线组合且为6-16个字符';
			return false;
		}

		if($password1 != $password2){
			$this->error = '两次密码不相同';
			return false;
		}

		$code = Q('post.code', '', 'strtoupper');
		if($code != session('code')){
			$this->error = '验证码不正确';
			return false;
		}

		return true;

	}

	/**
	 * [get_username 获得用户名]
	 * @param  [type] $uid [description]
	 * @return [type]      [description]
	 */
	public function get_username($uid){
		return $this->where(array('uid'=>$uid))->getField('nickname');
	}

	/**
	 * [get_data 获得所有用户信息]
	 * @return [type] [description]
	 */
	public function get_data($where=NULL){
		return $this->where($where)->findAll();
	}

	/**
	 * [del_data 删除用户]
	 * @param  [type] $uid [description]
	 * @return [type]      [description]
	 */
	public function del_data($uid){
		return  $this->where(array('uid'=>$uid))->delete();
	}

	/**
	 * [mdf_lock 锁定与解锁用户]
	 * @param  boolean $is_lock [description]
	 * @return [type]           [description]
	 */
	public function mdf_lock($is_lock=TRUE){
		$uid=Q('uid',0,'intval');
		if($is_lock){
			return $this->where(array('uid'=>$uid))->update(array('is_lock'=>1));
		}else{
			return $this->where(array('uid'=>$uid))->update(array('is_lock'=>0));
		}
	}

}
?>