<?php
/* 
* @Title:  [文章评论管理模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-24 17:21:48
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 16:41:08
* @Copyright:  [hn7m.com]
*/
class CommentModel extends Model
{
	public $table='comment';

	public $validate = array(
		array('comcon','nonull','评论内容不能为空',2,3)
		);

	//自动添加发布时间
	public $auto=array(
		array('addtime','_addtime','method',2,1)
		);
	protected function _addtime(){
		return time();
	}
	/**
	 * [total_comment 获得文章的评论总数]
	 * @param  [type] $aid [文章主键id]
	 * @return [type]      [description]
	 */
	public function total_comment($aid){
		return $this->where(array('article_aid'=>$aid))->count();
	}

	/**
	 * [get_arc_comment 获得一篇文章所有评论]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function get_arc_comment($aid,$order=NULL){
		return $this->where(array('article_aid'=>$aid))->order($order)->findAll();
	}

	/**
	 * [add_data 添加评论]
	 */
	public function add_data(){
		$this->data['user_uid']=Q('session.uid',NULL,'intval');  //此处应该是session里面的uid
		$this->data['article_aid']=Q('get.aid',0,'intval');//文章id,从ajax传递的地址而来
		return $this->add();
	}

	public function del_data($coid){
		return $this->where(array('coid'=>$coid))->delete();
	}
}
?>