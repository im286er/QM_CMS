<?php
/* 
* @Title:  [友情链接模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-24 23:13:19
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 19:25:12
* @Copyright:  [hn7m.com]
*/
class FlinkModel extends Model{
	public $table='flink';

	// //验证规则
	// public $validate=array(
	// 	array('url','nonull','网址不能为空',2,3),
	// 	array('url','http','网址格式不正确',2,3),
	// 	array('fname','nonull','网站名称不能为空',2,3),
	// 	array('sort','nonull','分类不能为空',2,3),
	// 	array('sort','regexp:/^[1-9]\d*$/','分类只能填写数字且不能是0或者以0开头',2,3),
	// 	array('msg','maxlen:200','简介不能超过200字',3,3)
	// 	);

	//自动添加发布时间
	public $auto=array(
		array('addtime','_addtime','method',2,1)
		);
	protected function _addtime(){
		return time();
	}
	/**
	 * [get_data 获得所有的数据]
	 * @param  [type] $field [description]
	 * @return [type]        [description]
	 */
	public function get_data($field=NULL){
		return $this->field($field)->findAll();
	}

	/**
	 * [get_one_data 获得一条数据]
	 * @param  [type] $field [description]
	 * @return [type]        [description]
	 */
	public function get_one_data($field=NULL){
		return $this->field($field)->find();
	}

	/**
	 * [add_data 增加链接]
	 */
	public function add_data(){
		$upload=new Upload();
		$upfiles=$upload->upload();

		//压缩略图信息到数据库flink表
		if(!empty($_FILES['logo']['name'])){
			//$upload->error是文件上传类的属性,详见手册106页
			if($upload->error){
				$this->error=$upload->error;
				return false;
			}
			$this->data['logo']=$upfiles[0]['thumb'];
		}

		return $this->add();
	}

	/**
	 * [del_data 删除链接]
	 * @return [type] [description]
	 */
	public function del_data($fid){
		//删除logo图片
		$logo=$this->where(array('fid'=>$fid))->getField('logo');
		$logo=ROOT_PATH.$logo;
		$path=trim($logo,strchr($logo,C('THUMB_ENDFIX'))).strrchr($logo, ".");
		!is_file($logo)||unlink($logo);
		!is_file($path)||unlink($path);
		return $this->where(array('fid'=>$fid))->delete();
	}

	/**
	 * [update_stat 修改是否显示的状态]
	 * @return [type] [description]
	 */
	public function update_stat(){
		$fid = Q('fid', 0, 'intval');
		$switch = Q('switch', 0, 'intval');
		if($switch){
			//执行修改
			$this->where(array('fid'=>$fid))->save(array('is_show'=>1));
		}else{
			$this->where(array('fid'=>$fid))->save(array('is_show'=>0));
		}	
		return true;
	}

	/**
	 * [update_data 修改友情链接]
	 * @return [type] [description]
	 */
	public function update_data(){
		$fid=Q('post.fid',0,'intval');
		$upload = new Upload();
		$upfiles = $upload->upload();

		//压缩略图信息到数据库article表
		if(!empty($_FILES['logo']['name'])){
			//p($_FILES);die();
			//$upload->error是文件上传类的属性,详见手册106页
			if($upload->error){
				$this->error=$upload->error;
				return false;
			}
			//如果上传了新的缩略图,则删除原缩略图
			
			$logo=$this->where(array('fid'=>$fid))->getField('logo');
			$logo=ROOT_PATH.$logo;
			$path=trim($logo,strchr($logo,C('THUMB_ENDFIX'))).strrchr($logo, ".");
			!is_file($logo)||unlink($logo);
			!is_file($path)||unlink($path);
		
			$this->data['logo']=$upfiles[0]['thumb'];
		}
		return $this->save();
	}
}
?>