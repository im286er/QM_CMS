<?php
/* 
* @Title:  [目录分类模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-19 16:09:45
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 20:42:04
* @Copyright:  [hn7m.com]
*/
class CategoryModel extends Model{
	//STEP ONE:指明该模型类用的是哪张表
	public $table = 'category';

	//STEP TWO:验证规则
	public $validate = array(
		array('cname','nonull','分类名称不能为空',2,3),
		array('ctitle','nonull','分类标题不能为空',2,3),
		array('cname','maxlen:20','分类名称不能超过20个字符',2,3),
		array('ctitle','maxlen:60','分类标题不能超过60个字符',2,3),
		array('ctitle','maxlen:60','分类标题不能超过60个字符',2,3),
		array('ckeywords','maxlen:255','分类关键字不能超过255个字符',3,3),
		array('cdes','maxlen:255','分类描述不能超过255个字符',3,3),
		array('htmldir','regexp:/^\w{1,20}$/','只能是1~20位只包含字母数字下划线',2,3)
		);

	//STEP THREE:添加数据
	public function add_data(){
		return $this->add();
	}

	/**
	 * [get_all 获得所有的数据,包括分页]
	 * @return [type] [description]
	 */
	// public function get_all(){
	// 	//统计
	// 	$total = $this->count();
	// 	//Page分页方法,参数==>$total总数, $row = ''每页多少条, $pageRow = ''多少个页码, $desc = ''分页文字设置,$setSelfPage = ''当前页, $customUrl = ''自定义 url, $pageNumLabel = '{page}页码变量 , 默认为 {page}'
	// 	$page = new Page($total,10,5);
	// 	//查询数据,注意执行$page里面的limit方法
	// 	//p($page->limit());die();
	// 	$data = $this->limit($page->limit())->findAll();
	// 	//获得分页样式，并且压入到$data
	// 	$data['page'] = $page->show(2);

	// 	return $data;

	// }
	public function get_all(){
		return $this->findAll();
	}

	/**
	 * [get_data 获得不包括分页的所有数据]
	 * @return [type] [description]
	 */
	public function get_data($field=NULL){
		return $this->field($field)->findAll();
	}

	/**
	 * [get_one_data 通过主键id获得单条数据]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_one_data($cid,$field=NULL){
		return $this->field($field)->where(array('cid'=>$cid))->find();
	}

	/**
	 * [update_stat 修改
	 * is_listhtml,is_arcthml,is_show他们的状态]
	 * @return [type] [description]
	 */
	public function update_stat(){
		$cid = Q('cid', 0, 'intval');
		$type = Q('type');
		$switch = Q('switch', 0, 'intval');

		switch ($type) {
			case 'list':
				if($switch){
					//执行修改
					$this->where(array('cid'=>$cid))->save(array('is_listhtml'=>1));
				}else{
					$this->where(array('cid'=>$cid))->save(array('is_listhtml'=>0));
				}
				break;
			case 'arc':
				if($switch){
					//执行修改
					$this->where(array('cid'=>$cid))->save(array('is_archtml'=>1));
				}else{
					$this->where(array('cid'=>$cid))->save(array('is_archtml'=>0));
				}
				break;
			case 'show':
				if($switch){
					//执行修改
					$this->where(array('cid'=>$cid))->save(array('is_show'=>1));
				}else{
					$this->where(array('cid'=>$cid))->save(array('is_show'=>0));
				}
				break;
		
		}

		return true;
	}

	/**
	 * [have_data 判断当前分类下面是否有文章或者是否有子分类]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function have_data($cid){
		$cid = $this->where(array('pid'=>$cid))->getField('cid');
		if($cid){
			$this->error = '请先删除子分类';
			return false;
		}

		// $aid = K('Article')->where(array('category_cid'=>$cid))->getField('aid');

		// if($aid){
		// 	$this->error = '请先删除该分类下面的文章';
		// 	return false;
		// }

		return true;
	}

	/**
	 * [del_data 根据主键id删除数据]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function del_data($cid){
		$this->delete($cid);
	}

	/**
	 * [update_data 执行修改动作]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function update_data($cid){
		//没有指定参数将以 $_POST 为参数进行更新，所以 $_POST 必须有值，并且必须有主键值，否则不进行更新，这样可以很好的保证数据不被误操作更新
		$this->where(array('cid'=>$cid))->update();
	}

	// cname cid 	pid

	// php 	1    0
	// ph      2    1
	// p       3    2
	// -p      4    3
	/**
	 * [get_son_id 获得自己所有子级id]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_son_id($cid){
		$cidArr = $this->field('cid,pid')->all();
		$tempArr = array();
		foreach ($cidArr as $v) {
			if($v['pid'] == $cid){
				$tempArr[] = $v['cid'];
				$tempArr = array_merge($tempArr, $this->get_son_id($v['cid']));
			}
		}
		return $tempArr;

	}

	/**
	 * [get_chose_cate 获得当前分类可以选择作为自己的父级]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_chose_cate($cid){
		$cidArr = $this->get_son_id($cid);
		$cidArr[] = $cid;
		$cidArr = implode(',', $cidArr);

		$sql = "SELECT `cid`,`cname` FROM " . C('DB_PREFIX') . "category WHERE cid NOT IN ($cidArr)";
	
		$data = $this->query($sql);
		return $data;
	}

	/**
	 * [get_pid 通过cid获得当前的pid]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_pid($cid){
		return $this->where(array('cid'=>$cid))->getField('pid');
	}

}
?>