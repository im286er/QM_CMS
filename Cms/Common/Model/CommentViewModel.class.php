<?php
/* 
* @Title:  [评论与用户视图表]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-28 16:00:27
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-29 20:25:35
* @Copyright:  [hn7m.com]
*/
class CommentViewModel extends ViewModel{
	//定义主表
	public $table='comment';
	//定义关联从表信息
	public $view=array(
		//从表一
		'user'	=>	array(
						'type'	=>INNER_JOIN,
						'on'	=>'comment.user_uid=user.uid'	
			)
		);

	/**
	 * [get_data 获得所有数据]
	 * @return [type] [description]
	 */
	public function get_data(){
		return $this->findAll();
	}

	/**
	 * [get_one_data 获得一篇文章的所有评论]
	 * @return [type] [description]
	 */
	public function get_arc_comment($aid,$order=NULL){
		return $this->where(array('article_aid'=>$aid))->order($order)->findAll();
	}
}
?>