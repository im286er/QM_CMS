<?php
/* 
* @Title:  [文章和分类的视图模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-21 19:35:41
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 21:46:39
* @Copyright:  [hn7m.com]
*/

class ArticleViewModel extends ViewModel
{
	
	//定义主表
	public $table='article';

	//定义关联从表信息
	public $view=array(
		//从表一
		'category'	=>	array(
						'type'	=>INNER_JOIN,
						'on'	=>'article.category_cid=category.cid'	
			),
		//从表二
		'article_data'	=>	array(
						'type'	=>INNER_JOIN,
						'on'	=>'article.aid=article_data.article_aid'	
			),
		);
		
	/**
	 * [get_data 获得所有的数据]
	 * @param  [type] $field [description]
	 * @return [type]        [description]
	 */
	public function get_data($recycle=NULL,$field=NULL)
	{
		$cid = Q('cid', 0, 'intval');

		$where = $cid ? array('cid'=>$cid) : NULL;
		$where['is_recycle']= (is_null($recycle))? 0:1;
		//p($where);die();
		return $this->where($where)->field($field)->order('aid asc')->findAll();
	}

	/**
	 * [get_one_data 获得一条数据]
	 * @param  [type] $cid [description]
	 * @return [type]      [description]
	 */
	public function get_one_data($aid=NULL){
		//return $this->where(array('aid'=>$aid))->find();
		return $this->find($aid);
	}

	/**
	 * [get_join_data 获得关联表信息]
	 * @param  [type] $table [被关联的表]
	 * @param  [type] $field [description]
	 * @return [type]        [description]
	 */
	public function get_join_data($table,$field=NULL){
		return $this->join($table)->field($field)->findAll();
	}
}
?>