<?php
/* 
* @Title:  [文章与标签中间表管理模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-20 20:01:13
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-25 09:30:10
* @Copyright:  [hn7m.com]
*/

class ArticleTagModel extends Model{
	public $table="article_tag";

	/**
	 * [del_data 删除标签中间表数据]
	 * @param  [type] $tid [description]
	 * @return [type]      [description]
	 */
	public function del_data($tid){
		$where =array('tag_tid'=>$tid);
		if ($this->where($where)->getField('tag_tid')) {
			$this->where($where)->delete();
		}
	}

	/**
	 * [get_aid 通过中间表标签tid获得文章aid]
	 * @param  [type] $tid [description]
	 * @return [type]      [description]
	 */
	public function get_aid($tid){
		$data=$this->where(array('tag_tid'=>$tid))->field('article_aid')->findAll();
		$temp=array();
		if (is_array($data)) {
			foreach ($data as $key => $value) {
				$temp[]=$value['article_aid'];
			}
		}
		return $temp;
	}

}
?>