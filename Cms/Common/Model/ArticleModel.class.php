<?php
/* 
* @Title:  [文章管理控制器]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-21 16:34:53
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-29 17:46:05
* @Copyright:  [hn7m.com]
*/
class ArticleModel extends Model{
	//设置表名
	public $table='article';
	//设置验证规则
	public $validate=array(
		array('title','nonull','标题不能为空',2,3),
		array('title','maxlen:80','标题不能超过80个字符',2,3),
		array('category_cid','regexp:/^[1-9]+$/','必须选择分类',2,3),
		array('digest','minlen:1','文章摘要不能少于1个字符',3,3),
		array('digest','maxlen:200','文章摘要不能超于200个字符',3,3)
		);
	//自动添加开始
	public $auto=array(
		array('sendtime','_sendtime','method',2,1),
		array('user_uid','_uid','method',2,3)
		);

	protected function _sendtime(){
		return time();
	}

	protected function _uid(){
		return Q('session.adminid');
	}
	//自动添加结束
	
	/**
	 * [add_data 添加文章]
	 */
	public function add_data(){
		//*****************************添加到article表开始*****************//
		$upload = new Upload();
		$upfiles = $upload->upload();
		//p($upfiles);die();

		//压缩略图信息到数据库article表
		if(!empty($_FILES['upload']['name'])){
			//p($_FILES);die();
			//$upload->error是文件上传类的属性,详见手册106页
			if($upload->error){
				$this->error=$upload->error;
				return false;
			}
			$this->data['thumb']=$upfiles[0]['thumb'];
		}

		//压文章属性到数据库article表
		$attr=is_array(Q('post.attr'))?implode(',', Q('attr')):NULL;
		$this->data['attr']=$attr;

		//压点击数到数据库article表
		$click=mt_rand(100,200);
		$this->data['click']=$click;

		//执行插入数据库操作并返回主键ID,用于article_tag表
		$aid = $this->add();

		//*****************************添加到Article表结束*****************//
		
		//*****************************添加到article_tag表开始*****************//
		$tidArr=Q('tag_tid');
		if (is_array($tidArr)) {
				foreach ($tidArr as $key => $value) {
				$data=array(
				"article_aid"	=>$aid,
				"tag_tid"		=>$value,
				"category_cid"	=>Q('category_cid', 0, 'intval')
				);
				K('ArticleTag')->add($data);
			}
		}
		
		//*****************************添加到article_tag表结束*****************//
		//*****************************添加到article_data表开始*****************//
		
		$arcDataModel = K('ArticleData');
		if(!$arcDataModel->create()){
			$this->error = $arcDataModel->error;
			return false;
		}

		$data=array(
			'keywords'	=>Q('keywords', NULL, 'htmlspecialchars'),
			'description'	=>Q('description', NULL, 'htmlspecialchars'),
			'content'	=>$_POST['content'],
			"article_aid"	=>$aid
			);
		$arcDataModel->add($data);
		//*****************************添加到article_data表结束*****************//
		return true;
	}

	/**
	 * [update_data 修改]
	 * @return [type] [description]
	 */
	public function update_data(){
		//*****************************更新article表开始*****************//
		$upload = new Upload();
		$upfiles = $upload->upload();
		$aid=Q('aid',0,'intval()');
		//压缩略图信息到数据库article表
		if(!empty($_FILES['upload']['name'])){
			//p($_FILES);die();
			//$upload->error是文件上传类的属性,详见手册106页
			if($upload->error){
				$this->error=$upload->error;
				return false;
			}
			//如果上传了新的缩略图,则删除原缩略图
			$thumb=$this->field('thumb')->find($aid);
			$thumb=$thumb['thumb'];
			$path=trim($thumb,strchr($thumb,C('THUMB_ENDFIX'))).strrchr($thumb, ".");
			!is_file($thumb)||unlink($thumb);
			!is_file($path)||unlink($path);
		
			$this->data['thumb']=$upfiles[0]['thumb'];
		}

		//压文章属性到数据库article表
		$attr=is_array(Q('post.attr'))?implode(',', Q('attr')):NULL;
		$this->data['attr']=$attr;

		//压点击数到数据库article表
		$click=mt_rand(100,200);
		$this->data['click']=$click;

		$this->save();
		//*****************************更新Article表结束*****************//
		
		//*****************************修改article_tag表开始*****************//
		//修改之前先删除原纪录
		K('ArticleTag')->where(array('article_aid'=>$aid))->delete();
		$tidArr=Q('tag_tid');
		if (is_array($tidArr)) {
				foreach ($tidArr as $key => $value) {
				$data=array(
				"article_aid"	=>$aid,
				"tag_tid"		=>$value,
				"category_cid"	=>Q('category_cid', 0, 'intval')
				);
				K('ArticleTag')->add($data);
			}
		}
		//*****************************修改article_tag表结束*****************//
		//*****************************修改article_data表开始*****************//
		
		$arcDataModel = K('ArticleData');
		if(!$arcDataModel->create()){
			$this->error = $arcDataModel->error;
			return false;
		}

		$data=array(
			'keywords'	=>Q('keywords', NULL, 'htmlspecialchars'),
			'description'	=>Q('description', NULL, 'htmlspecialchars'),
			'content'	=>$_POST['content'],
			"article_aid"	=>$aid
			);
		$arcDataModel->where(array('article_aid'=>$aid))->save();
		//*****************************修改article_data表结束*****************//
		return true;
		
	}

	/**
	 * [recycle 删除文章到回收站,即将article表中is_recycle字段的纪录值改为1]
	 * @return [type] [description]
	 */
	public function recycle($aid){
		return $this->where(array('aid'=>$aid))->save(array('is_recycle'=>1));
	}

	/**
	 * [recoveried 从回收站中还原]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function recoveried($aid){
		return $this->where(array('aid'=>$aid))->save(array('is_recycle'=>0));
	}

	/**
	 * [del_data 彻底删除数据]
	 * @return [type] [description]
	 */
	public function del_data($aid){
		//一定要先删除图片,再清空数据库
		//删除缩略图
		$thumb=$this->find($aid);
		$thumb=ROOT_PATH.$thumb['thumb'];
		$path=trim($thumb,strchr($thumb,C('THUMB_ENDFIX'))).strrchr($thumb, ".");
		!is_file($thumb)||unlink($thumb);
		!is_file($path)||unlink($path);
		//删除article表相关信息
		$this->where(array('aid'=>$aid))->delete();
		//删除article_data表相关信息
		K('ArticleData')->where(array('article_aid'=>$aid))->delete();
		//删除article_tag表相关信息
		K('ArticleTag')->where(array('article_aid'=>$aid))->delete();

		return true;
	}

	/**
	 * [get_click 通过文字主键id获得点击次数]
	 * @param  [type] $aid [description]
	 * @return [type]      [description]
	 */
	public function get_click($aid,$jia){
		if($jia){
			//点击次数加1
			$this->inc('click','aid='.$aid,1);
		}
		//返回点击次数
		return $this->where(array('aid'=>$aid))->getField('click');
	}
}
?>