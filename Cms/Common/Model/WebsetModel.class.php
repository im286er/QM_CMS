<?php
/* 
* @Title:  [网站配置管理模型]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-27 12:30:32
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-27 12:36:42
* @Copyright:  [hn7m.com]
*/
class WebSetModel extends Model{
	public $table ='config';

	/**
	 * [get_data 获得所有数据]
	 * @return [type] [description]
	 */
	public function get_data(){
		return $this->findAll();
	}

	/**
	 * [save_data 修改数据]
	 * @return [type] [description]
	 */
	public function save_data(){
		$coidArr=Q('post.coid');
		//循环完成修改
		foreach ($coidArr as $key => $value) {
			$this->where(array('coid'=>$key))->save(array('value'=>$value));
		}
	}


}
?>