<?php
/* 
* @Title:  [单入口文件]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-18 10:30:23
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-28 19:55:39
* @Copyright:  [hn7m.com]
*/
if(!is_file('./Install/Data/lock.php') && is_dir('./Install')){
	header('location:./Install/index.php');
}
//定义默认应用组名称常量
define('GROUP_NAME', 'Cms');
//定义默认应用组路径常量
define('GROUP_PATH', './Cms/');
//定义调试模式
define('DEBUG', TRUE);
//载入核心文件
require '../hdphp/hdphp/hdphp.php';
?>