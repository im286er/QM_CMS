<?php if(!defined('HDPHP_PATH'))EXIT;
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."article`");
$db->exe("CREATE TABLE `".$db_prefix."article` (
  `aid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文章表主键id',
  `title` char(120) NOT NULL DEFAULT '' COMMENT '文章标题',
  `click` mediumint(8) unsigned NOT NULL DEFAULT '0' COMMENT '点击次数',
  `sendtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发表时间',
  `updatetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间，自动更新',
  `thumb` varchar(100) NOT NULL DEFAULT '' COMMENT '缩略图',
  `is_recycle` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0为没有进入的回收站，1为在回收站中',
  `digest` varchar(255) NOT NULL DEFAULT '' COMMENT '文章摘要',
  `attr` set('图文','推荐','热门','置顶') DEFAULT NULL COMMENT '文章属性',
  `author` char(15) NOT NULL DEFAULT 'admin' COMMENT '作者',
  `category_cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类id',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属用户id',
  `source` varchar(30) NOT NULL DEFAULT '' COMMENT '文章来源',
  PRIMARY KEY (`aid`),
  KEY `fk_cmd_article_cms_category_idx` (`category_cid`),
  KEY `fk_article_user1_idx` (`user_uid`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COMMENT='文章表'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."article_data`");
$db->exe("CREATE TABLE `".$db_prefix."article_data` (
  `keywords` varchar(125) NOT NULL DEFAULT '' COMMENT '关键字',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
  `content` text COMMENT '文章内容',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属文章id',
  KEY `fk_article_data_article1_idx` (`article_aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章分离表'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."article_tag`");
$db->exe("CREATE TABLE `".$db_prefix."article_tag` (
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属文章id',
  `tag_tid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '所属标签id',
  `category_cid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '所属分类id',
  KEY `fk_article_tag_article1_idx` (`article_aid`),
  KEY `fk_article_tag_tag1_idx` (`tag_tid`),
  KEY `fk_article_tag_category1_idx` (`category_cid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='文章与标签关联表'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."category`");
$db->exe("CREATE TABLE `".$db_prefix."category` (
  `cid` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类表主键id',
  `cname` char(20) NOT NULL DEFAULT '' COMMENT '分类名称',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `ctitle` varchar(60) NOT NULL DEFAULT '' COMMENT '分类标题',
  `cdes` varchar(255) NOT NULL DEFAULT '' COMMENT '分类描述',
  `ckeywords` varchar(120) NOT NULL DEFAULT '' COMMENT '关键字',
  `csort` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '分类排序',
  `htmldir` varchar(50) NOT NULL DEFAULT '' COMMENT '静态目录',
  `is_listhtml` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '列表页是否生成静态',
  `is_archtml` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '内容页是否生成静态',
  `is_show` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '是否显示分类,0为不显示，1为显示',
  PRIMARY KEY (`cid`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COMMENT='分类表'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."comment`");
$db->exe("CREATE TABLE `".$db_prefix."comment` (
  `coid` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '评论主键id',
  `pid` smallint(5) unsigned NOT NULL DEFAULT '0' COMMENT '父级id',
  `comcon` varchar(255) NOT NULL DEFAULT '' COMMENT '评论内容',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '发表时间',
  `user_uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属用户uid',
  `article_aid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '所属文章aid',
  PRIMARY KEY (`coid`),
  KEY `fk_comment_user1_idx` (`user_uid`),
  KEY `fk_comment_article1_idx` (`article_aid`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='文章评论表'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."config`");
$db->exe("CREATE TABLE `".$db_prefix."config` (
  `coid` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(15) NOT NULL DEFAULT '' COMMENT '配置项名称',
  `value` varchar(70) NOT NULL DEFAULT '' COMMENT '配置值',
  `title` varchar(45) NOT NULL DEFAULT '' COMMENT '配置项标题',
  `des` varchar(255) NOT NULL DEFAULT '' COMMENT '配置项详细描述',
  `type_id` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '配置项类型id，相同类型id相同',
  PRIMARY KEY (`coid`)
) ENGINE=MyISAM AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COMMENT='站点配置'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."flink`");
$db->exe("CREATE TABLE `".$db_prefix."flink` (
  `fid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '友情链接主键id',
  `fname` varchar(30) NOT NULL DEFAULT '' COMMENT '友情链接名称',
  `msg` varchar(150) NOT NULL DEFAULT '' COMMENT '友情链接描述',
  `addtime` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '添加时间',
  `sort` smallint(5) unsigned NOT NULL DEFAULT '100' COMMENT '排序',
  `logo` varchar(80) NOT NULL DEFAULT '' COMMENT '友情链接Logo',
  `url` varchar(75) NOT NULL DEFAULT '' COMMENT '友情链接地址',
  `is_show` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '0为不显示，1为默认为显示',
  PRIMARY KEY (`fid`)
) ENGINE=MyISAM AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COMMENT='站点导航'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."tag`");
$db->exe("CREATE TABLE `".$db_prefix."tag` (
  `tid` smallint(5) unsigned NOT NULL AUTO_INCREMENT COMMENT '标签主键id',
  `tagname` char(26) NOT NULL DEFAULT '' COMMENT '标签名称',
  PRIMARY KEY (`tid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COMMENT='文章标签'");
$db->exe("DROP TABLE IF EXISTS `".$db_prefix."user`");
$db->exe("CREATE TABLE `".$db_prefix."user` (
  `uid` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户表主键id',
  `username` char(20) NOT NULL DEFAULT '' COMMENT '用户名',
  `nickname` varchar(25) NOT NULL DEFAULT '' COMMENT '昵称',
  `password` char(32) NOT NULL DEFAULT '' COMMENT '密码',
  `is_admin` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '0为普通用户，1为后台用户',
  `is_lock` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否锁定，0为没有锁定，1为锁定',
  `supper` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '超级管理员，不能被锁定，不能被删除,1为超级管理员，0为普通',
  `email` varchar(30) NOT NULL DEFAULT '' COMMENT '用户邮箱',
  PRIMARY KEY (`uid`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='用户表'");
