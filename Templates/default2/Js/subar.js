require(["$"],
function() {
    function t(t, e) {
        if (!$(this).data("ex") || !$(this).data("ey")) {
            var a = e ? $(e) : $(this),
            i = a.find(".arr");
            escape(i.html()).replace("%u", "&#x").toLocaleLowerCase() + ";",
            a.next().hasClass("group") && a.next().toggleClass("fold"),
            i.toggleClass("fold")
        }
    }
    function e(t, e) {
        var a = e ? $(e) : $(this),
        i = a.find(".arr");
        a.next().addClass("fold"),
        i.toggleClass("fold")
    }
    function a(t) {
        if (t.target != $(this).find(".option")[0]) {
            var a = $(this),
            i = $(".sub-bar .sbr-hd").height() + $(".sub-bar .sbr-bd").height();
            a.data("ex", t.pageX),
            a.data("ey", t.pageY),
            0 == t.button && (a.addClass("dragable"), $(".sub-bar").disableSelection(), $(document).delegate("body", "mousemove",
            function(t) {
                function s(t, e, a) {
                    t.css({
                        top: l
                    }).addClass("draging");
                    var i, s, n = a || e.index(t);
                    e.length,
                    n += Math.round((l - d) / c),
                    n > e.length - 1 && (n = length - 1),
                    o ? e.eq(n).before(g) : (0 >= n && (n = 1), i = d > l ? "prev": "next", s = e.eq(n)[i](), s.is("dd") ? s.is(":hidden") ? (s.prev().addClass("in"), s.find(".group-list").prepend(g)) : e.eq(n).prev().find(".group-list").append(g) : e.eq(n).before(g))
                }
                if ($(".in").removeClass("in"), a.not(".sub-cp").is("dt")) {
                    var o = !0;
                    $(".group-title").each(function() {
                        e(t, this)
                    })
                }
                var n = $(".sub-list"),
                r = a.parent(),
                l = 0,
                d = 0,
                p = (r.offset().top - n.offset().top, r.height(), n.find(".group-list").index(r), ".dragon"),
                u = o ? n.find("dt.dragon") : n.find(p).not(":hidden"),
                g = (u.index(a), r.find(p).index(a), u.length, a.parents(".sub-list").find(".puppet")[0] || $('<li class="puppet"></li>'));
                a.data("old_top", a.data("old_top") || a.offset().top - n.offset().top),
                a.data("dlt_top", a.data("dlt_top") || t.pageY - a.data("old_top")),
                d = a.data("old_top"),
                l = t.pageY - a.data("dlt_top"),
                0 > l && (l = 0),
                l > i && (l = i),
                s(a, u)
            }))
        }
    }
    function i(t) {
        t.parent().hasClass("sub-list") ? t[0].outerHTML = t[0].outerHTML.replace(/^\s*<li([\s\S]*)<\/li>$/gi, "<dt$1</dt>") : t.is("li") || (t[0].outerHTML = t[0].outerHTML.replace(/^\s*<dt([\s\S]*)<\/dt>$/gi, "<li$1</li>"))
    }
    function s() {
        var t = $(".sub-list"),
        e = {
            folders: [],
            root: []
        },
        a = t.find("dt.group-title"),
        i = $("dt.sub-cp");
        return a.each(function() {
            var t = $(this),
            a = t.find(".s-title .title").html(),
            i = t.next().find(".sub-cp"),
            s = {
                n: a,
                fs: []
            };
            i.each(function() {
                var t = $(this);
                t.attr("data-id") && s.fs.push(t.attr("data-id"))
            }),
            e.folders.push(s)
        }),
        i.each(function() {
            var t = $(this);
            t.attr("data-id") && e.root.push(t.attr("data-id"))
        }),
        e
    }
    function o(t) {
        var e, a = $('.group-title .s-title .title:contains("' + t + '")');
        return a.each(function() {
            var a = $(this);
            return a.html() == t ? (e = a, void 0) : void 0
        }),
        e
    }
    function n() {
        $.ajax({
            url: "/ajax/subscription/update",
            type: "post",
            data: s(),
            success: function() {}
        })
    }
    function r(t, e) {
        var a, i = "",
        s = "",
        o = "";
        "string" == typeof t ? (s = t, a = e, $.ajax({
            url: "/ajax/subscription/addfolder",
            type: "post",
            data: {
                name: s
            },
            success: function(t) {
                var e = $.parseJSON(t);
                console.info("~change data~", e),
                0 != e.c && a.attr("data-id", e.d.id)
            }
        })) : (a = t.parents(".group").prev(), o = t.attr("data-id"), t.is("dt") ? i = 0 : (i = a.attr("data-id"), s = a.find(".title").html()), $.ajax({
            url: "/ajax/subscription/changefolder",
            type: "post",
            data: {
                sid: o,
                fid: i,
                fname: s
            },
            success: function(t) {
                var e = $.parseJSON(t);
                console.info("~change data~", e),
                0 != e.c && a.attr("data-id", e.d.id)
            }
        }))
    }
    function l() {
        var t = $(this),
        e = $("#add_grp"),
        a = t.parents(".sub-cp"),
        s = a.attr("data-id");
        return e.show(),
        e.find(".grp-name").focus(),
        t.parent().hide(),
        e.on("click", ".done",
        function() {
            if ("" != $(this).siblings(".grp-name").val()) {
                var n, l, d, p = $(this),
                u = p.siblings(".grp-name").val(),
                c = a.parents(".group").prev().find(".s-title .title").html(),
                g = o(u, a),
                f = $('<li class="gmn-li"><a href="javascript:;" class="root title">根目录</a></li>'),
                h = a.find(".opt-list .group-menu");
                g ? g.parents(".group-title").next().find(".group-list").prepend(a) : (d = $(".sub-list .sub-cp .opt-list .group-menu"), d.each(function() {
                    if (this != h[0]) {
                        var t = $(this),
                        e = t.parents(".sub-cp"),
                        a = (e.attr("data-id"), $('<li class="gmn-li"><a href="javascript:;" class="title">' + u + "</a></li>"));
                        e.is("dt") ? t.prepend(a) : (console.log("%%%%"), t.find(".root").parent().after(a))
                    }
                }), n = $('<dt class="group-title dragon">                  <div class="h4-title s-title"><span class="arr icon-lianbo">&nbsp;</span><span class="title">' + u + '</span>                  </div>                  <div class="option" data-before="⚙"></div>                  <ul class="group-menu drp-menu" style="display: none;">                    <li class="l edit">重命名</li>                    <li class="l dismiss">解散分组</li>                    <li class="l new">+新建分组</li>                  </ul>                </dt>'), l = $('<dd class="group">                  <ul class="group-list">                    <li class="sub-cp xiedian" style="height:0"></li>                  </ul>                </dd>'), t.parents(".sub-list").prepend(n), l.find(".group-list").prepend(a), n.after(l)),
                a.is("dt") || f.find(".title").html(c).removeClass("root"),
                h.prepend(f),
                a[0] ? (i(a), r($('[data-id="' + s + '"]'))) : r(u, n),
                e.off("click", ".done").hide()
            }
        }).on("keyup", ".grp-name",
        function(t) {
            var e = t.keyCode,
            a = $(this),
            i = a.siblings(".done");
            13 == e && i.trigger("click")
        }),
        !1
    }
    function d(t) {
        t.stopPropagation();
        var e = $(this),
        a = $('<input type="text" id="editor" />'),
        i = e.parents(".group-title"),
        s = i.find(".title"),
        o = i.find(".title").html();
        a.val(o),
        s.hide(),
        e.parents(".drp-menu").hide().trigger("_hide"),
        e.parents(".group-title").find('.s-title').append(a),
        a.focus()
        e.parent().hide();
    }
    function p() {
        var t = $(this),
        e = t.parents(".group-title"),
        a = e.next().find(".group-list .sub-cp"),
        s = e.attr("data-id"),
        o = $(".group-title").index(e);
        $.ajax({
            url: "/ajax/subscription/delfolder",
            type: "post",
            data: {
                sid: s
            },
            success: function(t) {
                var s = $.parseJSON(t),
                n = $(".group-menu");
                $("dt.sub-cp").first().before(a),
                a.each(function() {
                    console.log($(this).find(".gmn-li")[0]),
                    $(this).find(".gmn-li").eq(0).remove(),
                    i($(this))
                }),
                n.each(function() {
                    var t = $(this),
                    e = o;
                    t.find(".root")[0] && (e += 1),
                    t.find(".gmn-li").eq(e).remove()
                }),
                e.remove(),
                0 == s.c
            }
        })
    } (function(t) {
        t.fn.disableSelection = function() {
            return this.attr("unselectable", "on").css("user-select", "none").on("selectstart", !1)
        }
    })(jQuery),
    $(document).on("toggle-fold", ".group-title", t),
    $(document).on("fold", ".group-title", e),
    $(document).delegate(".group-title", "click", t);
    var u = ".sub-list .dragon",
    c = $(u).height();
    $(document).delegate("body", "mouseup",
    function(t) {
        if ($(".dragable")[0]) {
            $(document).undelegate("body", "mousemove");
            var e = $(u).filter(".dragable"),
            a = 0,
            s = $(".sub-list").offset().top,
            o = e.next();
            e.data("ex") && e.data("ex", t.pageX - e.data("ex")) && console.log(e.data("ex")),
            e.data("ey") && e.data("eY", t.pageY - e.data("ey")) && console.log(e.data("ex")),
            e.removeClass("dragable").removeData("old_top").removeData("dlt_top"),
            e.hasClass("draging") && (a = $(".puppet").offset().top - s, $(".puppet:hidden")[0] && (a = $(".puppet").parents("dd").prev().offset().top - s), e.animate({
                top: a
            },
            110,
            function() {
                $(".puppet").after(e),
                o.hasClass("group") && e.after(o),
                e.removeClass("draging"),
                e.css({
                    top: 0
                }),
                $(".puppet").remove(),
                $(".in").removeClass("in"),
                i(e)
            }))
        }
    }),
    $(document).delegate(".drag-opt .lock", "click",
    function() {
        var t = $(this);
        t.html(),
        t.toggleClass("un"),
        t.hasClass("un") ? ($(document).delegate(u, "mousedown", a), t.attr("data-before", "&#x1f513;")) : ($(document).undelegate(u, "mousedown", a), t.attr("data-before", "&#x1f512;"), n())
    }),
    $(".sub-bar .drag-opt .lock").hover(function() {
        $(this).find(".hint").attr("class", "hint animated flipInY")
    },
    function() {
        $(this).find(".hint").attr("class", "hint animated flipOutY")
    }),
    $('.sub-bar .opt-list .s-a').hover(function(){
    	$(this).find('.group-menu').css('display','block')
    },function(){
    	$(this).find('.group-menu').css('display','none')
    }),
    $('.me-sub-list .sub-cp').hover(function(){
    	$(this).find('.option').show();
    },function(){
    	$(this).find('.option').hide();
    	$(this).find('.option').removeClass('on');
    	$(this).find('.option').removeClass('bottom');
    	$(this).find('.opt-list').hide();
    }),
    $(document).delegate(".sub-bar .sub-list .sub-cp .option", "click",
    function() {
        var t = $(this),
        e = t.siblings(".opt-list"),
        a = e.height() + (e.find(".group-menu li").length - 2) * $(".group-title.dragon").height(),
        i = t.parents(".sub-cp"),
        s = i.height(),
        o = i.offset().top + s - $(window).scrollTop(),
        n = $(window).height(),
        r = $(window).scrollTop(),
        l = e.find(".group-menu");
        l.attr("class", "group-menu drp-menu"),
        o - ($(".sub-bar").offset().top - r) > a + 20 && o + a > n ? (t.toggleClass("bottom"), e.css({
            bottom: "43px"
        }), l.addClass("bottom"), t.hasClass("bottom") && e.show() || e.hide()) : (t.toggleClass("on"), l.addClass("top"), e.css({
            bottom: ""
        }), t.hasClass("on") && e.show() || e.hide())
    }),
//    $(document).delegate('.sub-bar .s-title','click',function(){
//    	var t = $(this),
//    		e = t.parent().next();
//    	if(t.hasClass('s-title-hide')){
//    		e.show();
//    		t.removeClass('s-title-hide');
//    	}else{
//    		e.hide();
//    		t.addClass('s-title-hide');
//    	}
//    }),
    $(document).delegate(".sub-list .group-title .option", "click",
    function(t) {
        t.stopPropagation();
        var e = $(this),
        a = e.siblings(".group-menu");
        e.toggleClass("on"),
        a.toggle()
    }),
    _hide.push({
        ".option": ".group-title .drp-menu"
    }),
    $(".group-title .drp-menu").bind("_hide",
    function() {
        var t = $(this),
        e = t.siblings(".option");
        e.removeClass("on").removeClass("bottom")
    }),
    $(".sub-bar .sub-list .opt-list").live({
        mouseleave: function() {
            var t = $(this);
            t.hide(),
            t.siblings(".option").removeClass("on").removeClass("bottom")
        }
    }),
    $(document).delegate(".sub-bar .sub-list .sub-cp .opt-list .group-menu .gmn-li .title", "click",
    function() {
        var t = $(this),
        e = $(this).html(),
        a = ($('.group-title .s-title .title:contains("' + e + '")'), $(this).parents(".opt-list")),
        s = t.parents(".sub-cp"),
        n = s.attr("data-id"),
        l = s.parents(".group").prev(),
        d = l.find(".s-title .title").html(),
        p = $(".group-title").index(l),
        u = $('<li class="gmn-li"><a href="javascript:;" class="root title">根目录</a></li>');
        a.hide(),
        a.siblings(".option").removeClass("on").removeClass("bottom"),
        t.hasClass("root") ? (u.html(d).removeClass("root"), a.find(".gmn-li").eq(p).after(u), a.find(".group-menu .root").parent().remove(), $(".sub-bar .sub-list dt.sub-cp").eq(0).before(s)) : (o(e, s).parents(".group-title").next().find(".group-list").append(s), a.find(".group-menu .root")[0] ? (u.html(d).removeClass("root"), a.find(".gmn-li").eq(p).after(u).remove()) : (t.parent().remove(), a.find(".group-menu").prepend(u))),
        i(s);
        var c = $('[data-id="' + n + '"]');
        r(c)
    }),
    $(document).delegate("#add_grp .close", "click",
    function() {
        $("#add_grp").fadeOut(),
        $("#add_grp .grp-name").val("")
    }),
    $(document).delegate(".sub-bar .sub-list .sub-cp .opt-list .group-menu .gmn-li .new_group,.sub-list .group-title .group-menu .new", "click", l),
    $(document).delegate(".sub-list .group-title .group-menu .edit", "click", d).delegate(".sub-list .group-title .group-menu .dismiss", "click", p).on("title-change", "#editor",
    function() {
        var t = $(this),
        e = t.parents(".group-title").find(".title"),
        a = t.val(),
        i = t.parents(".group-title").attr("data-id");
        $.ajax({
            url: "/ajax/subscription/renamefolder",
            type: "post",
            data: {
                sid: i,
                name: a
            },
            success: function(i) {
                var s = $.parseJSON(i);
                0 != s.c && e.html(a),
                e.show(),
                t.remove()
            }
        })
    }).on("blur", "#editor",
    function() {
        var t = $(this),
        e = t.parents(".group-title").find(".title");
        return 1 == e.data("esc") ? (e.data("esc", 0), void 0) : (function(){
        	var a = t.val(),
            i = t.parents(".group-title").attr("data-id");
            $.ajax({
                url: "/ajax/subscription/renamefolder",
                type: "post",
                data: {
                    sid: i,
                    name: a
                },
                success: function(i) {
                    var s = $.parseJSON(i);
                    0 != s.c && e.html(a),
                    e.show(),
                    t.remove()
                }
            })
        }())
    }).on("click", "#editor",
    function(t) {
        t.stopPropagation()
    }).on("keyup", "#editor",
    function(t) {
        var e = t.keyCode,
        a = $(this),
        i = a.parents(".group-title").find(".title");
        27 == e && (i.data("esc", 1), a.remove(), i.show()),
        13 == e && a.remove()
    }),
    $(document).delegate(".sub-bar .sub-list .sub-cp .opt-list .cancel", "click",
    function() {
        var t = $(this),
        e = t.parents(".sub-cp"),
        a = e.attr("data-id");
        e.fadeOut(300),
        $.ajax({
            type: "post",
            url: "/ajax/subscription/cancel",
            data: {
                sid: a
            },
            success: function(t) {
                var a = $.parseJSON(t);
                a.c && e.remove()
            }
        })
    }),
    $(document).on('click',function(){
    	$('.sbr-bd .group-title .group-menu').hide();
    }),
    window.onbeforeunload = function() {
        $(".sub-bar .lock.un")[0] && n()
    }
});