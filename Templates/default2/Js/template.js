define(['mustache','handlebars'],function(){
  var temp = '\
  <div class="art-desc">\
    <h3 class="art-t"><a href="javascript:;" class="a art-title">{{{title}}}{{^title}}标题{{/title}}</a><span class="loading"></span></h3>\
    <p {{#if image_origin}} style="min-height:116px;" {{/if}}>\
    {{#if image_origin}}<img class="art-img" src="http://proxy.xgres.com/thumbImg.php?w=150&h=110&u={{image_origin}}" />{{/if}}\
    <span class="art-main">{{{description}}}</span>\
    </p>\
  </div>\
  <a class="v art-detail-btn" href="{{article_url}}" target="_blank">&nbsp;</a>\
  <div class="art-detail content-box">\
    <h3 class="art-t"><a href="javascript:;" class="art-title">{{{title}}}{{^title}}标题{{/title}}</a></h3>\
    <div class="art-detail-info">\
      <a class="a art-source" href="{{#_section_info}}{{xg_url}}{{/_section_info}}" target="_blank">{{source_name}}</a>\
      <a class="art-time" target="_blank">{{format_time}}</a>\
    </div>\
    <div class="art-content">{{{content}}}</div>\
  </div>\
  <div class="art_ft">\
  <div class="v art-pub">\
	<div class="v share"></div>\
	<div class="v {{#if is_favorite}}stared{{else}}star{{/if}}"></div>\
	<div class="v {{#if is_favorite}}marked{{else}}mark{{/if}}"></div>\
	<div class="v cmnt"><span class="num cmnt-num">{{comment_num}}</span></div>\
  </div>\
  <div class="v art-info">\
  	<a href="{{extra_url}}" class="original" target="_blank">查看原文</a>\
    <a class="a art-source" href="{{#_section_info}}{{xg_url}}{{/_section_info}}" target="_blank">{{source_name}}</a>\
    <a class="a art-time" href="{{extra_url}}" target="_blank">{{format_time}}</a>\
    {{#if cp}}\
    <div class="ops">\
    <a class="v art-del" href="/cp/delart?did={{_id}}_{{_section_info._id}}_{{_sid}}" onclick="if(!con())return false;" target="_blank">删除</a>\
    <a class="v art-edit" href="/cp/editart?did={{_id}}_{{_section_info._id}}" target="_blank">修改</a>\
    </div>\
    {{/if}}\
  </div>\
 </div>\
  <div class="cloudbtn art-fold">\
  收起\
  </div>\
  '
  , template = Handlebars.compile(temp);
  return template;
});