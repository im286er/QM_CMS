require(["domready"], function(t) {
	t(function() {
		require(["/static/v5/wheels/article/main.js", "$"], function(t) {
			if ($(".sub-list").delegate(".sub-cp,.group-title .s-title .title", "click", function(a) {
				var i = $(this),
					e = "",
					s = new t.collection,
					l = $("#add"),
					d = $('<span class="loading show-load"></span><span class="chunk-load">loading...</span>');
				if (i.attr("data-id") || i.parents(".group-title").attr("data-id"), i.attr("data-id")) e = i.attr("data-id"), $(".article").data("type", 0);
				else {
					var r = i.parents(".group-title");
					if (!r.next().find(".dragon")[0]) return;
					e = r.attr("data-id"), $(".article").data("type", 1)
				}
				$(".hot_title").html('');
				return i.data("ex") && i.data("ey") || i.hasClass("dragon") && (a.target == i.find(".option")[0] || $.contains(i.find(".opt-list")[0], a.target)) ? void 0 : (chunkLoading(".article"), $(".article .article-list").attr("data-id", e).html("").append(l), l.hide(), l.addClass("cloudbtn loading").removeClass("dis-btn show-load").html("查看更多"), $(window).scrollTop(0), $(".sub-list .sub-cp,.sub-list .group-title,.sub-list .group-title .s-title .title").filter(".cur").removeClass("cur"), i.addClass("cur").parents(".group-title").addClass("cur"), $("#add.loading").die(), new t.listView({
					el: $(".article .article-list"),
					collection: s
				}), s.fetch({
					data: {
						sid: e
					},
					reset: !0,
					success: function() {
						d.remove(), chunkLoaded(".article");
						if(i.children().length>0){
							if(i.find('span')[0].className == 'h4-title'){
								$(".hot_title").html(i.find('span').html());
							}
						}else{
							$(".hot_title").html(i.html());
						}
					}
				}), !1)
			}), $(".sub-list .sub-cp.dragon").length > 0) {
//				if($(".get_lianbo_hot").length==0){
//					var a = $(".sub-list .sub-cp.dragon").first();
//					a.is("dt") ? a.trigger("click") : a.parents(".group").prev().find(".s-title .title").trigger("click")
//				}
			} else $(".nofeed").addClass("show")
		})
	})
});