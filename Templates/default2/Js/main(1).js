define(["/static/v5/wheels/article/template.js", "backbone", "$"],
function(t) {
    $(".article").delegate(".art-pub .share", "click",
    function() {
        _hide.push({
            ".art-pub .share": ".share-list"
        });
        var t = $(this),
        e = $(".share-list"),
        a = e.parent(),
        i = t,
        n = t.parents(".entry").attr("_id"),
        l = t.offset().top,
        s = t.offset().left;
        if (t.parents(".entry").siblings(".entry").find(".art-pub .share").removeClass("cur"), t.toggleClass("cur"), !t.hasClass("cur")) return e.hide(),
        void 0;
        e.find("a").each(function() {
          var t = $(this);
          t.attr("href", t.attr("_href") + n)
	    });
//        for (e.find("a").each(function() {
//            var t = $(this);
//            t.attr("href", t.attr("_href") + n)
//        });
//        (i = i.parent()) && i[0] != a[0];) l += i.position().top,
//        s += i.position().left;
        return e.css({
            top: l+t.height()+6,
            left: s
        }).show(),
        !1
    }),
    $(".article").delegate(".art-pub .star,.art-pub .stared", "click",
    function() {
        checkLogin();
        var t = $(this),
        e = t.parents(".entry"),
        a = e.attr("_id"),
        i = "/ajax/favorite/add";
        t.hasClass("star") ? t.removeClass("star").addClass("stared") : (t.removeClass("stared").addClass("star"), i = "/ajax/favorite/cancel"),
        $.ajax({
            url: i,
            type: "post",
            data: {
                pid: a
            },
            success: function() {}
        })
    });
    $(".article").delegate(".art-pub .mark ,.art-pub .marked", "click",
    function() {
    	 checkLogin();
         var t = $(this),
         e = t.parents(".entry"),
         a = e.attr("doing_id"),
         i = "/doings/get-favorite-info";
         $.ajax({
             url: i,
             type: "post",
             data: {
        	 	doingsId:a
             },
             success: function(data) {
            	if(typeof(data) == 'string'){
            		data = $.parseJSON(data);
            	}
            	 if(data.d){
            		 data = data.d;
            	 }
            	 var modal;
                 var addTagTpl;
                 if (addTagTpl == null) {
                   addTagTpl = new JSmarty($templates.ADD_TAG_FOR_FAVDOING);
                   addTagTpl.compile();
                 }
            	 var tagsLink = "";
                 if(data.tags){
                	 tagsLink = data.tags.join(',');
                 }
                 
                 var tags = favTags.split(',');
                 var tagsHtml = "";
                 for (var i=0; i< tags.length;i++) {
                   if(i>50) break;//最多显示50个常用标签
                   tagsHtml+='<a href="javascript:;" class="tag-option">'+tags[i]+"</a>&nbsp;&nbsp;";
                 }
            	 modal = $dialog.modal('打标签', function(){
                     return addTagTpl.compiled({
                       myTags:tagsLink,
                       notes:data.note?data.note:"",
                       doingsId:a,
                       tagsHtml:tagsHtml
                     });
                   });
            	 modal.width("530");
            	 $('.tag-option').click(function(){
                     var tagName = $(this).text();
                     var tagArray = $('#fav-tag-input').val().split(",");
                     var newTagArray = [];
                     var flag = false;
                     for(var i=0 ;i<tagArray.length; i++) {
                       if($.trim(tagArray[i])==""){
                         continue;
                       }
                       if($.trim(tagArray[i])==tagName) {
                         flag = true;
                         continue;
                       }
                       newTagArray.push($.trim(tagArray[i]));
                     }
                     //							console.log(tagArray);
                     if(!flag){
                       newTagArray.push(tagName);
                     }
                     $('#fav-tag-input').val(newTagArray.join(','));
                   });
            	 $('#formUpdateTag').submit(function(e){
            		 $.ajax({
                         url: e.target.action,
                         type: "post",
                         data: $(this).serialize(),
                         dataType: 'json',
                     	 success: function(data) {
            			 	modal.close();
            			 	t.hasClass("mark") ? t.removeClass("mark").addClass("marked") : "";
                         }
            		 })
                     return false;
                   });
             }
         })
    });
    var e = Backbone.Model.extend({
        idAttribute: "_id",
        url: "/ajax/article/get",
        validate: function() {},
        parse: function(t) {
            return t.d ? t.d: t
        }
    }),
    a = Backbone.View.extend({
        tagName: "li",
        template: t,
        events: {
            "click .art-desc .art-t": "unfold",
            "click .art-detail .art-t": "fold",
            "click .art-fold": "fold",
            "dblclick .art-desc": "unfold",
            "dblclick .art-detail": "fold",
            "click .cmnt": "doComment"
        },
        initialize: function() {
            _.bindAll(this, "render", "unfold"),
            this.model.bind("change", this.render),
            $(this.el).attr("_id", this.model.get("_id")),
            $(this.el).attr("doing_id", this.model.get("article_id")),
            $(this.el).addClass("entry")
        },
        render: function() {
            var t = this.template(this.model.attributes);
            return $(this.el).html(t),
            this
        },
        unfold: function() {
            function t() {
                a.removeClass("show-load"),
                e.render(),
                $(e.el).addClass("detail")
                //$(e.el).find(".art-detail").toggleClass("animated fadeInDown")
            }
            var e = this,
            a = $(e.el).find(".loading");
            "" == $(e.el).find(".art-detail .art-content").html() ? (a.addClass("show-load"), this.model.fetch({
                data: {
                    id: $(this.el).attr("_id")
                },
                success: t
            })) : t()
        },
        fold: function() {
            var t = "fixed" == $(".header").css("position") ? 100 : 18;
            $(this.el).removeClass("detail")
           // $(this.el).find(".art-desc").toggleClass("animated fadeInUp"),
            $(window).scrollTop() > $(this.el).offset().top && $(window).scrollTop($(this.el).offset().top - t)
        },
        doComment: function() {
            var t = $(this.el),
            e = $(this.el).next();
            return e.hasClass("comment") ? (e.find(".cmnt-content").val(""), e.toggle(), void 0) : (require(["/static/v5/wheels/comment/main.js", "autogrow"],
            function(e) {
                t.after(e.cmnt);
                var a = t.next(),
                i = t.next().find(".cmnt-content"),
                n = t.next().find(".text-arrow"),
                l = t.next().find(".cmnt-list");
                i.autogrow({
                    animate: !1
                }),
                i.focus(function() {
                    n.addClass("cur")
                }),
                i.blur(function() {
                    n.removeClass("cur")
                }),
                l.attr("data-id", t.attr("_id"));
                var s = new e.collection;
                new e.listView({
                    collection: s,
                    el: l
                }),
                t.attr("_id"),
                i.change(function() {
                    "" != i.val() ? a.find(".cmnt-btn").removeClass("dis-btn").addClass("seabtn") : a.find(".cmnt-btn").removeClass("seabtn").addClass("dis-btn")
                })
            }), void 0)
        }
    }),
    i = Backbone.Collection.extend({
        model: e,
        url: "/ajax/article/getlist",
        parse: function(t) {
            if (!t || !t.d || !t.d.length) {
                var e = $("#add");
                return e.html("木有了……去看看别的频道吧"),
                e.addClass("dis-btn").removeClass("cloudbtn loading"),
                void 0
            }
            $('.nofeed').hide();
            
            return t.d
        }
    });
    window.articleList = new i;
    var n = Backbone.View.extend({
        tagName: "ul",
        events: {},
        collection: "",
        callback: function() {},
        page: 0,
        initialize: function() {
            var t = this;
            $(this.el).attr("id", "doings-list"),
            $(this.el).addClass("doings_list"),
            $(document).on("click", "#add.loading",
            function() {
                var e = $(this),
                a = $(t.el);
                e.hasClass("show-load") || (e.addClass("show-load dis-btn").removeClass("cloudbtn"), 1 == a.parent().data("type") ? t.loadMoreByMaxId() : t.load())
            }),
            t.collection.bind("reset",
            function() {
                "function" == typeof t.callback && t.callback(),
                t.page += 1,
                t.addCollection(t.collection),
                $(t.el).parents(".article").find(".ht-title").removeClass("show-load"),
                $("#add").show()
            })
        },
        render: function(e) {
            var a = t(e);
            $(this.el).html(a)
        },
        addOne: function(t) {
            if (0 == t.attributes.c) return console.log("wrong article data"),
            void 0;
            var e = new a({
                model: t
            });
            $(this.el).append(e.render().el)
        },
        addCollection: function(t) {
            var e = this;
            t.each(function(t) {
                e.addOne(t)
            })
        },
        loadMoreByPage: function() {},
        loadMoreByMaxId: function() {
            var t = this;
            t.collection.fetch({
                data: {
                    lt: "max",
                    sid: $(t.el).attr("data-id"),
                    lid: $(t.el).find("li").last().attr("_id")
                },
                reset: !0,
                success: function() {
                    $("#add").hasClass("loading") && $("#add").removeClass("show-load dis-btn").addClass("cloudbtn")
                }
            })
        },
        loadMoreBySinceId: function() {},
        load: function() {
            var t = this;
            t.collection.fetch({
                data: {
                    lid: t.page,
                    sid: $(t.el).attr("data-id")
                },
                reset: !0,
                success: function() {
                    $("#add").hasClass("loading") && $("#add").removeClass("show-load dis-btn").addClass("cloudbtn")
                }
            })
        }
    });
    return {
        model: e,
        collection: i,
        view: a,
        listView: n
    }
});