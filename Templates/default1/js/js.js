$(function(){
	// 顶部二级菜单
	$('#top_dark_box #top_dark #menu>li').hover(function(){
		$(this).find('.top_menu').addClass('cur');
		$(this).find('.second').show();
	},function(){
		$(this).find('.top_menu').removeClass('cur');
		$(this).find('.second').hide();
	})

		

	// 中间区域活跃读者效果
	$('#main .center .head_portrait li').hover(function(){
		$(this).find('.hide_box').show();
		$('#main .center .head_portrait li').stop();
		$(this).css('z-index','5').fadeTo(0,1).siblings('li').css('z-index','0').fadeTo(400,0.5);
	},function(){
		$(this).find('.hide_box').hide();
		$(this).siblings('li').fadeTo(0,1);
	})

	// 内容页留言列表隔行变色
	$('#page_main .left .msg_list li:odd').css('background','#FDFDFD');
	$('#page_main .left .msg_list li>.reply_box .reply_box:even').css('background','#fff');
	//标签颜色相关
	(function(){
		var tagColors = ['#000000','#fb5088','#72f6f4','#f5756e','#4a4451','#e764c0'];
		$('#tagBox a').each(function(i){
			var color = tagColors[i%tagColors.length];
			$(this)[0].style.background = color;
		})
	
	})()	
	
})

function showLoginBox(){
	$('.login_hide_box').show();
	$('.reg_hide_box').hide();
}

function showRegBox()
{
	$('.login_hide_box').hide();
	$('.reg_hide_box').show();
}

//HDJS自动验证
$(function(){
	$('#regForm').validate({
		username_reg:{
			//指定规则
			rule:{
				required:true,
				user 	:'5,20',
				ajax 	:APP+'&c=Register&m=check_user'
			},
			//错误规则
			error:{
				required:'用户名必填',
				user 	:'5-20个字符(数字/字母/下划线)',
				ajax 	:'用户名已存在!'
			},
			//正常提示
			message:'5-20个字符(数字/字母/下划线)',
			//成功提示
			success:'ok'
		},
		nickname:{
			//指定规则
			rule:{
				required:true,
				minlen:2,
				maxlen:10
			},
			//错误规则
			error:{
				required:'昵称必填',
				minlen:'最少2个字符',
				maxlen:'最多10个字符'
			},
			//正常提示
			message:'2-10个字符(中英文/数字)',
			//成功提示
			success:'ok'
		},
		password_reg1:{
			//指定规则
			rule:{
				required:true,
				minlen:6,
				maxlen:16,
				regexp:/^\w+$/
			},
			//错误规则
			error:{
				required:'密码必填',
				minlen:'最少6个字符',
				maxlen:'最多16个字符',
				regexp:'6-16个字符(不包含空格)'
			},
			//正常提示
			message:'6-16个字符(不包含空格)',
			//成功提示
			success:'ok'
		},
		password_reg2:{
			//指定规则
			rule:{
				required:true,
				confirm:'password_reg1'
			},
			//错误规则
			error:{
				required:'重复输入一次密码',
				confirm:'两次密码不相同'
			},
			//正常提示
			message:'重复输入一次密码',
			//成功提示
			success:'ok'
		},
		code:{
			//指定规则
			rule:{
				required:true,
				ajax 	:APP+'&c=Register&m=check_code'
			},
			//错误规则
			error:{
				required:'验证码必填',
				ajax:'验证码不正确'
			},
			//正常提示
			message:'验证码不区分大小写',
			//成功提示
			success:'ok'
		},
	})
})

//异步检测是否登陆
$(function(){
	$.ajax({
		url:APP+'&c=Login&m=check_login',
		dataType:'html',
		success:function(data){
			$('#loginInfo').html(data);
		}
	})
})
	