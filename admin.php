<?php
/* 
* @Title:  [这里并不是又定义了一个入口文件,只是为了更好的用户体验.让用户在地址栏输入?admin.php就可以访问后台而创的伪后台入口]
* @Author: {chenlei} [chenleib5@126.com]
* @Date:   2014-03-19 14:50:25
* @Last Modified by:   Administrator
* @Last Modified time: 2014-03-23 22:40:03
* @Copyright:  [hn7m.com]
*/
//因为前后台公用一个单入口,所以这里引入后台路径

header('location:./index.php?a=Admin');

?>